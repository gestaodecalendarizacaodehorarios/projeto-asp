﻿using Nager.Date;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using filtrandoResultadosBD.Models;
using System.IO;

namespace Horarios.ExcelHorario
{
    public class InfoParaOHorario
    {
        public int IdFormador { get; set; }
        public string NomeFormador { get; set; }
        public int NmrTotalDeHorasCurso { get; set; }
        public string RegimeTurma { get; set; }
        public DateTime DataDeInicio { get; set; }
        public int MesDeFerias { get; set; }
        public string NomeDaTurma { get; set; }

        public static List<InfoParaOHorario> Get(int idFormador)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
                return (from form in mcdh.formadors
                        join fm in mcdh.formador_modulo
                        on form.id equals fm.id_formador
                        join tm in mcdh.turmas
                        on fm.id_turma equals tm.id
                        join cur in mcdh.cursoes
                        on tm.id_curso equals cur.id
                        where form.id == idFormador
                        select new InfoParaOHorario
                        {
                            IdFormador = idFormador,
                            NomeFormador = form.nome,
                            NmrTotalDeHorasCurso = cur.TotalHoras,
                            RegimeTurma = tm.regime.regime1,
                            NomeDaTurma = tm.nome,
                            DataDeInicio = tm.data_inicio,
                            MesDeFerias = tm.mes_ferias
                        }).ToList();
        }



    }

    #region AnteriorCalendarioProvisorio

    //public class CalendarioProvisorio
    //{

    //    private static void KillSpecificExcelFileProcess(string excelFileName)
    //    {
    //        var processes = from p in Process.GetProcessesByName("EXCEL")
    //                        select p;

    //        foreach (var process in processes)
    //        {
    //            if (process.MainWindowTitle == "Microsoft Excel - " + excelFileName)
    //                process.Kill();
    //        }
    //    }
    //    //return the date between two dates
    //    public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
    //    {
    //        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
    //            yield return day;
    //    }

    //    //print it or whatever
    //    //foreach (DateTime day in EachDay(StartDate, EndDate))
    //    public static void Executa(InfoParaOHorario info)
    //    {
    //        //CriaArquivoEExcelESalvaCValidacoesObrigatorias(DateTime.Parse("26/10/2019"));

    //        if (info.RegimeTurma == "Diurno")
    //        {
    //            CriaHorarioDiurno(info);
    //        }
    //        if (info.RegimeTurma == "Tarde")
    //        {
    //            CriaHorarioDaTarde(info);
    //        }
    //        if (info.RegimeTurma == "Pós-laboral")
    //        {
    //            CriaHorarioPosLaboral(info);
    //        }


    //        #region antigo

    //        //if(info.RegimeTurma == "Diurno")
    //        //{ 
    //        //    Task tsk1 = Task.Run(()=> { 
    //        //       CriaHorarioDiurno(info);
    //        //    });
    //        //    tsk1.Wait();
    //        //}
    //        //if (info.RegimeTurma == "Tarde")
    //        //{
    //        //    Task tsk2 = Task.Run(() =>
    //        //    {
    //        //        CriaHorarioDaTarde(info);
    //        //    });
    //        //    tsk2.Wait();
    //        //}
    //        //if (info.RegimeTurma == "Pós-laboral")
    //        //{
    //        //    Task tsk3 = Task.Run(() =>
    //        //    {
    //        //        CriaHorarioPosLaboral(info);
    //        //    });
    //        //    tsk3.Wait();
    //        //}
    //        #endregion

    //        //Task.WaitAll(tsk1);
    //        //LeCorExcelTOP();
    //    }

    //    public static void LeCorExcelTOP()
    //    {
    //        Excel.Application xlsApp = new Excel.Application();
    //        Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Open(@"E:\IN LIVE\GIT\GITLAB\projeto-asp\ASP.NET Core Web Application\projetoCDH\testesExcel\Disponibilidade-formador.xlsx");
    //        Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1];
    //        var gray = xlsWorkSheet.Cells[10, 3].Interior.Color;
    //        //11184814 - cinza
    //        //255 - vermelho
    //        Console.WriteLine("Pegado");

    //    }

    //    #region criaHorarioGeral
    //    //public static void CriaArquivoEExcelESalvaCValidacoesObrigatorias(DateTime DataInicio)
    //    //{
    //    //    //calculaDataFinal
    //    //    DateTime DataFinalCurso = CalculaDataFinalTPSI(DataInicio);



    //    //    //mata processo
    //    //    KillSpecificExcelFileProcess("Criadela.xlsx");
    //    //    if (File.Exists(Directory.GetCurrentDirectory() + "/Criadela.xlsx"))
    //    //        File.Delete(Directory.GetCurrentDirectory() + "/Criadela.xlsx");

    //    //    Excel.Application xlsApp = new Excel.Application();
    //    //    Excel.Workbook xlsWorkBook;
    //    //    Excel.Worksheet xlsWorkSheet;

    //    //    xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
    //    //    xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

    //    //    xlsWorkSheet.Name = "Sheet um";

    //    //    //if (File.Exists(Directory.GetCurrentDirectory() + "/Criadela.xlsx"))
    //    //    //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ Criadela.xlsx");
    //    //    //Adiciona sheet

    //    //    try
    //    //    {

    //    //        #region Row 1
    //    //        xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
    //    //        xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

    //    //        var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
    //    //        FirstCell.RowHeight = 69;


    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

    //    //        xlsWorkSheet.Columns[1].ColumnWidth = 15;
    //    //        xlsWorkSheet.Columns[2].ColumnWidth = 15;

    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
    //    //             Excel.XlVAlign.xlVAlignCenter;
    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
    //    //             Excel.XlVAlign.xlVAlignCenter;

    //    //        xlsWorkSheet.Cells[1, 1] = "ID\ndo\nFormador";

    //    //        xlsWorkSheet.Cells[1, 2] = "Horas";

    //    //        xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //    //        xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

    //    //        #endregion
    //    //        #region Row 2

    //    //        xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

    //    //        #endregion
    //    //        #region Row 3

    //    //        xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //    //        xlsWorkSheet.Cells[3, 1].VerticalAlignment =
    //    //        Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
    //    //        xlsWorkSheet.Cells[3, 1] = "Nome\ndo\nFormador";

    //    //        #endregion

    //    //        #region Conf horas

    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
    //    //             Excel.XlVAlign.xlVAlignCenter;
    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
    //    //             Excel.XlVAlign.xlVAlignCenter;

    //    //        xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
    //    //        xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
    //    //        xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
    //    //        xlsWorkSheet.Cells[6, 2] = "19:15/23:25";


    //    //        #endregion


    //    //        xlsWorkSheet.Columns.AutoFit();


    //    //        int column = 3;

    //    //        for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //    //        {
    //    //            #region Row 1

    //    //            xlsWorkSheet.Cells[1, column] = day;
    //    //            xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion


    //    //            #region Row 2
    //    //            DayOfWeek dofw = day.DayOfWeek;
    //    //            xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
    //    //            xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion

    //    //            #region Row 3
    //    //            xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion

    //    //            #region Row 4
    //    //            xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion

    //    //            #region Row 5
    //    //            xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion 

    //    //            #region Row 6
    //    //            xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //    //            #endregion

    //    //            ////Usar cellls
    //    //            //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //    //            column++;
    //    //        }

    //    //        xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

    //    //        column = 3;
    //    //        for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //    //        {
    //    //            if (DateSystem.IsWeekend(day, CountryCode.PT) || DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
    //    //            {
    //    //                xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //    //            }

    //    //            ////Usar cellls
    //    //            //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //    //            column++;
    //    //        }

    //    //        xlsWorkSheet.Columns.AutoFit();

    //    //        xlsWorkBook.SaveAs(Directory.GetCurrentDirectory() + "/Criadela.xlsx");

    //    //        xlsWorkBook.Close(true);
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //    //        xlsApp.Quit();
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //    //    }
    //    //    catch (Exception)
    //    //    {
    //    //        xlsWorkBook.Close(true);
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //    //        xlsApp.Quit();
    //    //        System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //    //        throw;
    //    //    }
    //    //}

    //    #endregion

    //    #region Cria Horario Diurno P formador
    //    public static void CriaHorarioDiurno(InfoParaOHorario info = null)
    //    {
    //        string pathDiurno = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/{info.NomeFormador}/Diurno";
    //        if (!File.Exists(pathDiurno))
    //            System.IO.Directory.CreateDirectory(pathDiurno);

    //        //mata processo
    //        KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
    //        if (File.Exists($"{pathDiurno}/_{info.NomeDaTurma}.xlsx"))
    //            File.Delete($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");

    //        //calculaDataFinal
    //        DateTime DataFinalCurso = CalculaDataFinal(info);

    //        Excel.Application xlsApp = new Excel.Application();
    //        Excel.Workbook xlsWorkBook;
    //        Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

    //        xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
    //        xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

    //        xlsWorkSheet.Name = $"{info.NomeDaTurma}";

    //        //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx"))
    //        //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDiurno.xlsx");
    //        //Adiciona sheet

    //        try
    //        {

    //            #region Row 1
    //            //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

    //            var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
    //            FirstCell.RowHeight = 69;

    //            //xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

    //            xlsWorkSheet.Columns[1].ColumnWidth = 15;
    //            xlsWorkSheet.Columns[2].ColumnWidth = 15;

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

    //            xlsWorkSheet.Cells[1, 2] = "Horas";

    //            xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //            xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

    //            #endregion
    //            #region Row 2

    //            xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
    //            xlsWorkSheet.Cells[8, 2] = "Indisponível";
    //            xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
    //            xlsWorkSheet.Cells[9, 2] = "Disponível";
    //            xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";



    //            #endregion
    //            #region Row 3

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();
    //            xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[3, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
    //            xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
    //            xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[8, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            //-Turma -\nTPSIP_11_18
    //            xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";


    //            #endregion

    //            #region Conf horas

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
    //            xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
    //            xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
    //            xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

    //            #endregion

    //            xlsWorkSheet.Columns.AutoFit();


    //            int column = 3;

    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                #region Row 1

    //                xlsWorkSheet.Cells[1, column] = day;
    //                xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 2
    //                DayOfWeek dofw = day.DayOfWeek;
    //                xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
    //                xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 3
    //                xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 4
    //                xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 5
    //                xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion 

    //                #region Row 6
    //                xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "5296274";

    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                //xlsWorkSheet.Range[xlsWorkSheet.Cells[5, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                // segundaDuasGoras= xlsWorkSheet.Cells[2, column]
    //                if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
    //                {


    //                    if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
    //                    {
    //                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
    //                        day = day.AddDays(daysUntilMonday);
    //                        column += daysUntilMonday;
    //                    }
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                }

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }


    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                if (DateSystem.IsWeekend(day, CountryCode.PT))
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                column++;
    //            }


    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "11184814";

    //            xlsWorkSheet.Columns.AutoFit();

    //            xlsWorkBook.SaveAs($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");

    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //        }
    //        catch (Exception)
    //        {
    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //            throw;
    //        }
    //        //Console.WriteLine("\n\n--Horário diurno criado! --\n");
    //    }
    //    #endregion
    //    #region Cria Horario da tarde P formador
    //    public static void CriaHorarioDaTarde(InfoParaOHorario info = null)
    //    {
    //        //calculaDataFinal
    //        DateTime DataFinalCurso = CalculaDataFinal(info);
    //        string pathTarde = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/{info.NomeFormador}/Tarde";
    //        if (!File.Exists(pathTarde))
    //            System.IO.Directory.CreateDirectory(pathTarde);

    //        //mata processo
    //        KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
    //        if (File.Exists($"{pathTarde}/_{info.NomeDaTurma}.xlsx"))
    //            File.Delete($"{pathTarde}/_{info.NomeDaTurma}.xlsx");

    //        Excel.Application xlsApp = new Excel.Application();
    //        Excel.Workbook xlsWorkBook;
    //        Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

    //        //xlsWorkSheet.Protect(false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);

    //        xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
    //        xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

    //        xlsWorkSheet.Name = $"{info.NomeDaTurma}";

    //        //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx"))
    //        //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDaTarde.xlsx");
    //        //Adiciona sheet

    //        try
    //        {

    //            #region Row 1
    //            //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

    //            var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
    //            FirstCell.RowHeight = 69;


    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

    //            xlsWorkSheet.Columns[1].ColumnWidth = 15;
    //            xlsWorkSheet.Columns[2].ColumnWidth = 15;

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

    //            xlsWorkSheet.Cells[1, 2] = "Horas";

    //            xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //            xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

    //            #endregion
    //            #region Row 2

    //            xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
    //            xlsWorkSheet.Cells[8, 2] = "Indisponível";
    //            xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
    //            xlsWorkSheet.Cells[9, 2] = "Disponível";
    //            xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";
    //            #endregion
    //            #region Row 3

    //            xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[3, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
    //            xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
    //            xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[8, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            //-Turma -\nTPSIP_11_18
    //            xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";

    //            #endregion

    //            #region Conf horas

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
    //            xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
    //            xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
    //            xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

    //            #endregion

    //            xlsWorkSheet.Columns.AutoFit();


    //            int column = 3;

    //            //CRIA HORARIO TD
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                #region Row 1

    //                xlsWorkSheet.Cells[1, column] = day;
    //                xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 2
    //                DayOfWeek dofw = day.DayOfWeek;
    //                xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
    //                xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 3
    //                xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 4
    //                xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 5
    //                xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion 

    //                #region Row 6
    //                xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

    //            column = 3;
    //            //retira feriados do horario da tarde
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {

    //                if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
    //                {

    //                    if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
    //                    {
    //                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
    //                        day = day.AddDays(daysUntilMonday);
    //                        column += daysUntilMonday;
    //                    }
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                }

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }

    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                if (DateSystem.IsWeekend(day, CountryCode.PT))
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                column++;
    //            }


    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "11184814";

    //            xlsWorkSheet.Columns.AutoFit();

    //            xlsWorkBook.SaveAs($"{pathTarde}/_{info.NomeDaTurma}.xlsx");

    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //        }
    //        catch (Exception)
    //        {
    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //            throw;
    //        }
    //        //Console.WriteLine("\n\n--Horário da tarde criado! --\n");
    //    }
    //    #endregion
    //    #region Cria Horario pós-laboral P formador
    //    public static void CriaHorarioPosLaboral(InfoParaOHorario info = null)
    //    {

    //        //calculaDataFinal
    //        DateTime DataFinalCurso = CalculaDataFinal(info);
    //        string pathPLaboral = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/{info.NomeFormador}/Pós-laboral";
    //        if (!File.Exists(pathPLaboral))
    //            System.IO.Directory.CreateDirectory(pathPLaboral);

    //        //mata processo
    //        KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
    //        if (File.Exists($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx"))
    //            File.Delete($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx");

    //        Excel.Application xlsApp = new Excel.Application();
    //        Excel.Workbook xlsWorkBook;
    //        Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

    //        xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
    //        xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

    //        xlsWorkSheet.Name = $"{info.NomeDaTurma}";

    //        //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx"))
    //        //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioPosLaboral.xlsx");
    //        //Adiciona sheet

    //        try
    //        {

    //            #region Row 1
    //            //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

    //            var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
    //            FirstCell.RowHeight = 69;


    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

    //            xlsWorkSheet.Columns[1].ColumnWidth = 15;
    //            xlsWorkSheet.Columns[2].ColumnWidth = 15;

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

    //            xlsWorkSheet.Cells[1, 2] = "Horas";

    //            xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //            xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

    //            #endregion
    //            #region Row 2

    //            xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
    //            xlsWorkSheet.Cells[8, 2] = "Indisponível";
    //            xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
    //            xlsWorkSheet.Cells[9, 2] = "Disponível";
    //            xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";
    //            #endregion
    //            #region Row 3

    //            xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[3, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
    //            xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
    //            xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[8, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            //-Turma -\nTPSIP_11_18
    //            xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";


    //            #endregion

    //            #region Conf horas

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
    //            xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
    //            xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
    //            xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

    //            #endregion

    //            xlsWorkSheet.Columns.AutoFit();


    //            int column = 3;

    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                #region Row 1

    //                xlsWorkSheet.Cells[1, column] = day;
    //                xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 2
    //                DayOfWeek dofw = day.DayOfWeek;
    //                xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
    //                xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 3
    //                xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 4
    //                xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 5
    //                xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion 

    //                #region Row 6
    //                xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[6, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {

    //                if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
    //                {
    //                    if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
    //                    {
    //                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
    //                        day = day.AddDays(daysUntilMonday);
    //                        column += daysUntilMonday;
    //                    }
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                }

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }

    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                if (DateSystem.IsWeekend(day, CountryCode.PT))
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                column++;
    //            }
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[5, column - 1]].Interior.Color = "11184814";

    //            xlsWorkSheet.Columns.AutoFit();

    //            xlsWorkBook.SaveAs($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx");

    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //        }
    //        catch (Exception)
    //        {
    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //            throw;
    //        }
    //        //Console.WriteLine("\n\n--Horario Pós-laboral criado! --\n");
    //    }
    //    #endregion


    //    #region Cria horário Diurno

    //    public static void HorarioDiurno(InfoParaOHorario info = null)
    //    {
    //        string pathDiurno = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/Diurnos";
    //        if (!File.Exists(pathDiurno))
    //            System.IO.Directory.CreateDirectory(pathDiurno);


    //        #region k process
    //        //mata processo
    //        KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
    //        if (File.Exists($"{pathDiurno}/_{info.NomeDaTurma}.xlsx"))
    //            File.Delete($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");
    //        #endregion

    //        //calculaDataFinal
    //        DateTime DataFinalCurso = CalculaDataFinal(info);

    //        Excel.Application xlsApp = new Excel.Application();
    //        Excel.Workbook xlsWorkBook;
    //        Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

    //        xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
    //        xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

    //        xlsWorkSheet.Name = $"{info.NomeDaTurma}";

    //        //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx"))
    //        //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDiurno.xlsx");
    //        //Adiciona sheet

    //        try
    //        {

    //            #region Row 1
    //            //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

    //            var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
    //            FirstCell.RowHeight = 69;

    //            //xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

    //            xlsWorkSheet.Columns[1].ColumnWidth = 15;
    //            xlsWorkSheet.Columns[2].ColumnWidth = 15;

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

    //            xlsWorkSheet.Cells[1, 2] = "Horas";

    //            xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //            xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

    //            #endregion
    //            #region Row 2

    //            xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
    //            xlsWorkSheet.Cells[8, 2] = "Indisponível";
    //            xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
    //            xlsWorkSheet.Cells[9, 2] = "Disponível";
    //            xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //            xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";



    //            #endregion
    //            #region Row 3

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();
    //            xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[3, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            //string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
    //            //xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
    //            xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Cells[8, 1].VerticalAlignment =
    //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

    //            //-Turma -\nTPSIP_11_18
    //            //xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";


    //            #endregion

    //            #region Conf horas

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
    //                 Excel.XlVAlign.xlVAlignCenter;

    //            xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
    //            xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
    //            xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
    //            xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

    //            #endregion

    //            xlsWorkSheet.Columns.AutoFit();


    //            int column = 3;

    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                #region Row 1
    //                xlsWorkSheet.Cells[1, column] = day;
    //                xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 2
    //                DayOfWeek dofw = day.DayOfWeek;
    //                xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
    //                xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 3
    //                xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 4
    //                xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                #region Row 5
    //                xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion 

    //                #region Row 6
    //                xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
    //                #endregion

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }

    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "5296274";

    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                //xlsWorkSheet.Range[xlsWorkSheet.Cells[5, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                // segundaDuasGoras= xlsWorkSheet.Cells[2, column]
    //                if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
    //                {


    //                    if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
    //                    {
    //                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
    //                        day = day.AddDays(daysUntilMonday);
    //                        column += daysUntilMonday;
    //                    }
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                }

    //                ////Usar cellls
    //                //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
    //                column++;
    //            }


    //            column = 3;
    //            for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
    //            {
    //                if (DateSystem.IsWeekend(day, CountryCode.PT))
    //                    xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
    //                column++;
    //            }


    //            xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "11184814";

    //            xlsWorkSheet.Columns.AutoFit();

    //            xlsWorkBook.SaveAs($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");

    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //        }
    //        catch (Exception)
    //        {
    //            xlsWorkBook.Close(true);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
    //            xlsApp.Quit();
    //            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
    //            xlsApp = null; // Set each COM Object to null

    //            GC.Collect(); // Start .NET CLR Garbage Collection
    //            GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
    //            throw;
    //        }
    //        //Console.WriteLine("\n\n--Horário diurno criado! --\n");
    //    }

    //    #endregion

    //    #region Cria horário da tarde
    //    #endregion

    //    #region Cria horário pós-laboral
    //    #endregion

    //    public static void CriaHorarioTardePFormadores(InfoParaOHorario[] arrayInfo)
    //    {
    //        foreach (InfoParaOHorario item in arrayInfo)
    //        {
    //            CriaHorarioDaTarde(item);
    //        }
    //    }




    //    public static DateTime CalculaDataFinal(InfoParaOHorario info)
    //    {
    //        //(1775 / 6)
    //        //    296 dias de tpsi;


    //        var daysToAdd = info.RegimeTurma == "Diurno" ? (info.NmrTotalDeHorasCurso / 7) : (info.RegimeTurma == "Tarde") ? info.NmrTotalDeHorasCurso / 6 : (info.RegimeTurma == "Pós-laboral") ? info.NmrTotalDeHorasCurso / 4 : info.NmrTotalDeHorasCurso / 0;
    //        DateTime dataFinal = info.DataDeInicio;

    //        var weeks = DateSystem.GetWeekendProvider(CountryCode.PT);

    //        while (daysToAdd > 0)
    //        {
    //            dataFinal = dataFinal.AddDays(1);

    //            if (!weeks.IsWeekend(dataFinal) &&
    //                !DateSystem.IsPublicHoliday(dataFinal, CountryCode.PT) && dataFinal.Month != info.MesDeFerias)
    //            {
    //                daysToAdd -= 1;
    //            }
    //        }

    //        return dataFinal;
    //        //Data FINAL 29 / 01 / 2021 c ferias
    //    }

    //}
    
    #endregion

    public class InfoParaUmHorario
    {
        public string NomeDaTurma { get; set; }
        public DateTime DataDeInicio { get; set; }
        public string RegimeTurma { get; set; }
        public int NmrTotalDeHorasCurso { get; set; }
        public int MesDeFerias { get; set; }


        private void KillSpecificExcelFileProcess(string excelFileName)
        {
            var processes = from p in Process.GetProcessesByName("EXCEL")
                            select p;

            foreach (var process in processes)
            {
                if (process.MainWindowTitle == "Microsoft Excel - " + excelFileName)
                    process.Kill();
            }
        }
        public DateTime CalculaDataFinal(InfoParaUmHorario info)
        {
            //(1775 / 6)
            //    296 dias de tpsi;


            var daysToAdd = info.RegimeTurma == "Diurno" ? (info.NmrTotalDeHorasCurso / 7) : (info.RegimeTurma == "Tarde") ? info.NmrTotalDeHorasCurso / 6 : (info.RegimeTurma == "Pós-laboral") ? info.NmrTotalDeHorasCurso / 4 : info.NmrTotalDeHorasCurso / 0;
            DateTime dataFinal = info.DataDeInicio;

            var weeks = DateSystem.GetWeekendProvider(CountryCode.PT);

            while (daysToAdd > 0)
            {
                dataFinal = dataFinal.AddDays(1);

                if (!weeks.IsWeekend(dataFinal) &&
                    !DateSystem.IsPublicHoliday(dataFinal, CountryCode.PT) && dataFinal.Month != info.MesDeFerias)
                {
                    daysToAdd -= 1;
                }
            }

            return dataFinal;
            //Data FINAL 29 / 01 / 2021 c ferias
        }

        #region Horario Diurno
        public string HorarioDiurno(InfoParaUmHorario info = null)
        {
            string pathDiurno = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/Diurnos";
            if (!File.Exists(pathDiurno))
                System.IO.Directory.CreateDirectory(pathDiurno);


            #region k process
            //mata processo
            KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
            if (File.Exists($"{pathDiurno}/_{info.NomeDaTurma}.xlsx"))
                File.Delete($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");
            #endregion

            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinal(info);

            Excel.Application xlsApp = new Excel.Application();
            xlsApp.Visible = true; 
            Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Add() as Excel.Workbook;
            Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1] as Excel.Worksheet;

            //xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            //xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = $"{info.NomeDaTurma}";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDiurno.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;

                //xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9, 2] = "Disponível";
                xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";



                #endregion
                #region Row 3

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();
                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
                //xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //-Turma -\nTPSIP_11_18
                //xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";


                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1
                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "5296274";

                column = 3;
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    //xlsWorkSheet.Range[xlsWorkSheet.Cells[5, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    // segundaDuasGoras= xlsWorkSheet.Cells[2, column]
                    if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
                    {


                        if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
                        {
                            int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
                            day = day.AddDays(daysUntilMonday);
                            column += daysUntilMonday;
                        }
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    }

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }


                column = 3;
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    if (DateSystem.IsWeekend(day, CountryCode.PT))
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    column++;
                }


                xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs($"{pathDiurno}/_{info.NomeDaTurma}.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                throw;
            }
            return $"{pathDiurno}/_{info.NomeDaTurma}.xlsx";
            //Console.WriteLine("\n\n--Horário diurno criado! --\n");
        }
        #endregion
        #region Horario da Tarde
        public string HorarioDaTarde(InfoParaUmHorario info = null)
        {
            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinal(info);
            string pathTarde = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/Tardes";
            if (!File.Exists(pathTarde))
                System.IO.Directory.CreateDirectory(pathTarde);

            //mata processo
            KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
            if (File.Exists($"{pathTarde}/_{info.NomeDaTurma}.xlsx"))
                File.Delete($"{pathTarde}/_{info.NomeDaTurma}.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

            //xlsWorkSheet.Protect(false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = $"{info.NomeDaTurma}";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDaTarde.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9, 2] = "Disponível";
                xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";
                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
                //xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //-Turma -\nTPSIP_11_18
                //xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";

                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                //CRIA HORARIO TD
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1],xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
                //formata as datas
                xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

                column = 3;
                //retira feriados do horario da tarde
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {

                    if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
                    {

                        if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
                        {
                            int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
                            day = day.AddDays(daysUntilMonday);
                            column += daysUntilMonday;
                        }
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    }

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                column = 3;
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    if (DateSystem.IsWeekend(day, CountryCode.PT))
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    column++;
                }


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs($"{pathTarde}/_{info.NomeDaTurma}.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                throw;
            }
            return $"{pathTarde}/_{info.NomeDaTurma}.xlsx";
            //Console.WriteLine("\n\n--Horário da tarde criado! --\n");
        }
        #endregion
        #region Horario pós-laboral
        public string HorarioPosLaboral(InfoParaUmHorario info = null)
        {

            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinal(info);
            string pathPLaboral = $"{Directory.GetCurrentDirectory()}/ExcelHorarios/Pós-laborais";
            if (!File.Exists(pathPLaboral))
                System.IO.Directory.CreateDirectory(pathPLaboral);

            //mata processo
            KillSpecificExcelFileProcess($"_{info.NomeDaTurma}.xlsx");
            if (File.Exists($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx"))
                File.Delete($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet = new Excel.Worksheet();

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = $"{info.NomeDaTurma}";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioPosLaboral.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                //xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = $"-Turma -\n{info.NomeDaTurma}";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9, 2] = "Disponível";
                xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";
                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //string[] splitadoNameBySpace = info.NomeFormador.Split(' ');
                //xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //-Turma -\nTPSIP_11_18
                //xlsWorkSheet.Cells[7, 1] = $"ID\n{info.IdFormador}";


                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, column]].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range[xlsWorkSheet.Cells[6, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

                column = 3;
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {

                    if (DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == info.MesDeFerias)
                    {
                        if (day.Day == 1 && !DateSystem.IsWeekend(day, CountryCode.PT))
                        {
                            int daysUntilMonday = ((int)DayOfWeek.Monday - (int)day.DayOfWeek + 7) % 7;
                            day = day.AddDays(daysUntilMonday);
                            column += daysUntilMonday;
                        }
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    }

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                column = 3;
                for (var day = info.DataDeInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    if (DateSystem.IsWeekend(day, CountryCode.PT))
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    column++;
                }
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[5, column - 1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs($"{pathPLaboral}/_{info.NomeDaTurma}.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                throw;
            }

            return $"{pathPLaboral}/_{info.NomeDaTurma}.xlsx";
            //Console.WriteLine("\n\n--Horario Pós-laboral criado! --\n");
        }
        #endregion

        #region Cria Horário dos formadores diurnos

        public void CriaHorarioParaOsFormadoresDiurnos(formador[] formador, string pathExcelPPaste)
        {

            foreach (var item in formador)
            {
                Excel.Application xlsApp = new Excel.Application();
                Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Open(pathExcelPPaste);
                Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1];
                try
                {
                    string pathDiurno = $"{Directory.GetCurrentDirectory()}/Formadores/{item.nome}/Diurno";
                if (!File.Exists(pathDiurno))
                    System.IO.Directory.CreateDirectory(pathDiurno);

          

                string[] splitadoNameBySpace = item.nome.Split(' ');
                xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //-Turma -\nTPSIP_11_18
                xlsWorkSheet.Cells[7, 1] = $"ID\n{item.id}";


                xlsWorkBook.SaveAs($"{pathDiurno}/_{item.username}_{Guid.NewGuid()}.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                xlsApp = null; // Set each COM Object to null

                GC.Collect(); // Start .NET CLR Garbage Collection
                GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
               

                }
                catch (Exception)
                {
                    xlsWorkBook.Close(true);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                    xlsApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    xlsApp = null; // Set each COM Object to null
                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                    throw;
                }
            }
        }



        #endregion
        #region horário dos formadores da tarde
        public void CriaHorarioParaOsFormadoresDaTarde(formador[] formador, string pathExcelPPaste)
        {

            foreach (var item in formador)
            {
                Excel.Application xlsApp = new Excel.Application();
                Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Open(pathExcelPPaste);
                Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1];
                try
                {
                    string pathTarde = $"{Directory.GetCurrentDirectory()}/Formadores/{item.nome}/Tarde";
                    if (!File.Exists(pathTarde))
                        System.IO.Directory.CreateDirectory(pathTarde);

                    string[] splitadoNameBySpace = item.nome.Split(' ');
                    xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                    xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                    xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                    //-Turma -\nTPSIP_11_18
                    xlsWorkSheet.Cells[7, 1] = $"ID\n{item.id}";


                    xlsWorkBook.SaveAs($"{pathTarde}/_{item.username}_{Guid.NewGuid()}.xlsx");

                    xlsWorkBook.Close(true);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                    xlsApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    xlsApp = null; // Set each COM Object to null

                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish


                }
                catch (Exception)
                {
                    xlsWorkBook.Close(true);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                    xlsApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    xlsApp = null; // Set each COM Object to null
                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                    throw;
                }
            }
        }

        #endregion
        #region horário dos formadores pós-laborais
        public void CriaHorarioParaOsPosLaborais(formador[] formador, string pathExcelPPaste)
        {

            foreach (var item in formador)
            {
                Excel.Application xlsApp = new Excel.Application();
                Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Open(pathExcelPPaste);
                Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1];
                try
                {
                    string pathLaboral = $"{Directory.GetCurrentDirectory()}/Formadores/{item.nome}/Pós-laboral";
                    if (!File.Exists(pathLaboral))
                        System.IO.Directory.CreateDirectory(pathLaboral);

                    string[] splitadoNameBySpace = item.nome.Split(' ');
                    xlsWorkSheet.Cells[3, 1] = $"{splitadoNameBySpace[0]}\n{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}";

                    xlsWorkSheet.Range[xlsWorkSheet.Cells[7, 1], xlsWorkSheet.Cells[9, 1]].Merge();
                    xlsWorkSheet.Cells[8, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    xlsWorkSheet.Cells[8, 1].VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                    //-Turma -\nTPSIP_11_18
                    xlsWorkSheet.Cells[7, 1] = $"ID\n{item.id}";


                    xlsWorkBook.SaveAs($"{pathLaboral}/_{item.username}_{Guid.NewGuid()}.xlsx");

                    xlsWorkBook.Close(true);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                    xlsApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    xlsApp = null; // Set each COM Object to null

                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish


                }
                catch (Exception)
                {
                    xlsWorkBook.Close(true);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                    xlsApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                    xlsApp = null; // Set each COM Object to null
                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish
                    throw;
                }
            }
        }

        #endregion


        //var daysToAdd = info.RegimeTurma == "Diurno" ? (info.NmrTotalDeHorasCurso / 7) : (info.RegimeTurma == "Tarde") ? info.NmrTotalDeHorasCurso / 6 : (info.RegimeTurma == "Pós-laboral") ? info.NmrTotalDeHorasCurso / 4 : info.NmrTotalDeHorasCurso / 0;
        //DateTime dataFinal = info.DataDeInicio;
        //public static InfoParaOHorario GetHorario(int idTurma)
        //{
        //    using (ModelCDHContext mcdh = new ModelCDHContext())
        //        return (from form in mcdh.turmas
        //                select new InfoParaOHorario
        //                {

        //                }).ToList();
        //}
    }
}
