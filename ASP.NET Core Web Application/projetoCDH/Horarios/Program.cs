﻿using Horarios.ExcelHorario;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using filtrandoResultadosBD.Models;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace Horarios
{
    class Program
    {
        static void Main(string[] args)
        {

            //string[] splitadoNameBySpace = ipoh[0].NomeFormador.Split(' ');

            //Console.WriteLine($"First Name: {splitadoNameBySpace[0]}, Last Name:{splitadoNameBySpace[splitadoNameBySpace.Length - 1]}");
            //Console.WriteLine("Turmas presente:");
            //Console.WriteLine("--");

            //foreach (var item in ipoh)
            //{
            //    Console.WriteLine($"Nome da turma: {item.NomeDaTurma}\tRegime Turma: {item.RegimeTurma}");
            //    Console.WriteLine($"Data de início da turma: {item.DataDeInicio.ToString("dd/MM/yyyy")}\tTotal de Horas: {item.NmrTotalDeHorasCurso}");
            //    Console.WriteLine("--");
            //}


            #region Gera Exceis de um formador

            //List<InfoParaOHorario> ipoh = InfoParaOHorario.Get(6);
            //Console.WriteLine($"Formador \"{ipoh[0].NomeFormador}\"");
            //Console.WriteLine("Para gerar: ");
            //Console.WriteLine("---");
            //foreach (var item in ipoh)
            //{
            //    Console.WriteLine();
            //    Console.WriteLine($"Nome da Turma:{item.NomeDaTurma}\nRegime:{item.RegimeTurma}");
            //    Console.WriteLine();
            //    Console.WriteLine("---");
            //}


            //List<Task> tasks = new List<Task>();

            //foreach (var item in ipoh)
            //{
            //    tasks.Add(Task.Run(() =>
            //    {
            //        CalendarioProvisorio.Executa(item);
            //    }));
            //}

            //Task t = Task.WhenAll(tasks);

            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            //Console.WriteLine("--Gerando os horários--");
            //Console.WriteLine("Tempo de duração:");
            //TimeSpan ts;

            //while (!t.IsCompleted)
            //{
            //    ts = sw.Elapsed;
            //    Console.Write(ts.Seconds + "s ");
            //    System.Threading.Thread.Sleep(1000);
            //}
            ////tsk.Wait();
            //sw.Stop();
            //Console.WriteLine();
            //Console.WriteLine();
            //Console.WriteLine("\n\nHorários gerados:");
            //Console.WriteLine();
            //Console.WriteLine();
            //foreach (var item in ipoh)
            //{
            //    Console.WriteLine();
            //    Console.WriteLine($"Turma: \"{item.NomeDaTurma}\"\tRegime \"{item.RegimeTurma}\" (*Criado com sucesso!!*)");
            //    Console.WriteLine();
            //    Console.WriteLine("---");
            //}



            #endregion
            #region Cria um Horário

            Stopwatch sw = new Stopwatch();
            sw.Start();
            InfoParaUmHorario infUmHorario = new InfoParaUmHorario();
            //infUmHorario.HorarioDiurno(new InfoParaUmHorario
            //{
            //    DataDeInicio = DateTime.Parse("10/31/2019"),
            //    IdTurma = 2,
            //    NmrTotalDeHorasCurso = 1375,
            //    MesDeFerias = 8,
            //    NomeDaTurma = "Uma turma qq",
            //    RegimeTurma = "Diurno"
            //});

            //28/01/2020
            string pathDiurno = infUmHorario.HorarioDiurno(new InfoParaUmHorario
            {
                //DataDeInicio = DateTime.ParseExact("1/11/2019", "MM/dd/yyyy", CultureInfo.InvariantCulture),
                DataDeInicio = DateTime.UtcNow,
                NmrTotalDeHorasCurso = 2000,
                MesDeFerias = 3,
                NomeDaTurma = "Uma turma Diurna",
                RegimeTurma = "Diurno"

            });

            using (ModelCDHContext mdch = new ModelCDHContext())
            {
                formador[] fm = mdch.formadors.ToArray();
                infUmHorario.CriaHorarioParaOsFormadoresDiurnos(fm, pathDiurno);

            }


            Console.WriteLine("--Gerando o horário--");
            Console.WriteLine("Tempo de duração:");
            TimeSpan ts = sw.Elapsed;
            sw.Stop();
            Console.WriteLine(ts.TotalSeconds);
            Console.WriteLine();
            Console.WriteLine($"\n\nHorários gerados");
            Console.WriteLine();
            Console.WriteLine();

            #endregion


            Console.ReadLine();

        }
    }


}
