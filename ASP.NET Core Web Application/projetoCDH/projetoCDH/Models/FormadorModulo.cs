﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class FormadorModulo
    {
        public int IdFormador { get; set; }
        public int IdModulo { get; set; }
        public int IdTurma { get; set; }

        public Formador IdFormadorNavigation { get; set; }
        public Modulo IdModuloNavigation { get; set; }
        public Turma IdTurmaNavigation { get; set; }
    }
}
