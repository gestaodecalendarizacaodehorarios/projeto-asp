﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class Turma
    {
        public Turma()
        {
            FormadorModulo = new HashSet<FormadorModulo>();
            HorarioTurmas = new HashSet<HorarioTurmas>();
        }

        public int Id { get; set; }
        public int? IdRegime { get; set; }
        public int? IdCurso { get; set; }
        public string Nome { get; set; }
        public DateTime DataInicio { get; set; }
        public int MesFerias { get; set; }
        public string Coordenador { get; set; }

        public Curso IdCursoNavigation { get; set; }
        public Regime IdRegimeNavigation { get; set; }
        public ICollection<FormadorModulo> FormadorModulo { get; set; }
        public ICollection<HorarioTurmas> HorarioTurmas { get; set; }
    }
}
