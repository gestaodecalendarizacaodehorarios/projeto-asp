﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace projetoCDH.Models
{
    public partial class ModelCDHContext : DbContext
    {
        public ModelCDHContext()
        {
        }

        public ModelCDHContext(DbContextOptions<ModelCDHContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<Disponibilidade> Disponibilidade { get; set; }
        public virtual DbSet<Formador> Formador { get; set; }
        public virtual DbSet<FormadorModulo> FormadorModulo { get; set; }
        public virtual DbSet<Grupo> Grupo { get; set; }
        public virtual DbSet<HorarioTurmas> HorarioTurmas { get; set; }
        public virtual DbSet<Horas> Horas { get; set; }
        public virtual DbSet<HorasRegime> HorasRegime { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<OrdemModCat> OrdemModCat { get; set; }
        public virtual DbSet<Regime> Regime { get; set; }
        public virtual DbSet<Turma> Turma { get; set; }
        public virtual DbSet<Notificacao> Notificacao { get; set; }
        public virtual DbSet<PlanoFunc> PlanoFunc { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source='registar.no-ip.org, 1435';User ID=TPSIP1118_igor;Password=aN22#2019;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlanoFunc>(entity =>
            {

                entity.ToTable("plano_func");
                entity.HasKey(e => new { e.Data });

                entity.Property(e => e.Data)
                .HasColumnName("data")
                .HasColumnType("date");

                entity.Property(e => e.Disp).HasColumnName("disp");


            });

            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.ToTable("categoria");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdGrupo).HasColumnName("id_grupo");

                entity.Property(e => e.Nome)
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.Property(e => e.Ordem).HasColumnName("ordem");

                entity.HasOne(d => d.IdGrupoNavigation)
                    .WithMany(p => p.Categoria)
                    .HasForeignKey(d => d.IdGrupo)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK__categoria__id_gr__18178C8A");
            });

            modelBuilder.Entity<Curso>(entity =>
            {
                entity.ToTable("curso");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Nome)
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.Property(e => e.Sigla)
                    .IsRequired()
                    .HasColumnName("sigla")
                    .HasMaxLength(25);

                entity.Property(e => e.TotalHoras)
                .IsRequired()
                .HasColumnName("TotalHoras");
            });

            modelBuilder.Entity<Notificacao>(entity =>
            {
                entity.ToTable("Notificacao");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdFormador)
                      .HasColumnName("id_formador");

                entity.Property(e => e.Turma)
                      .HasColumnName("turma")
                      .HasMaxLength(200);

                entity.Property(e => e.PathExcel)
                      .HasColumnName("path_excel");

                entity.Property(e => e.HoraCriada)
                      .HasColumnName("hora_criada")
                      .HasColumnType("datetime2");


                entity.Property(e => e.Visualizacao)
                      .HasColumnName("visualizacao")
                      .HasColumnType("bit");

                entity.Property(e => e.Inserido)
                      .HasColumnName("inserido")
                      .HasColumnType("bit");

                entity.Property(e => e.PathExcelInserido)
                      .HasColumnName("path_excel_inserido");

                entity.HasOne(d => d.IdFormadorNavigation)
                      .WithMany(p => p.Notificacao)
                      .HasForeignKey(d => d.IdFormador)
                      .OnDelete(DeleteBehavior.ClientSetNull)
                      .HasConstraintName("FK__notificacao__id___7993056A");

            });

            modelBuilder.Entity<Disponibilidade>(entity =>
            {
                entity.HasKey(e => new { e.Data, e.IdFormador });

                entity.ToTable("disponibilidade");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("date");

                entity.Property(e => e.IdFormador).HasColumnName("id_formador");

                entity.Property(e => e.H0830).HasColumnName("h0830");

                entity.Property(e => e.H0930).HasColumnName("h0930");

                entity.Property(e => e.H1040).HasColumnName("h1040");

                entity.Property(e => e.H1140).HasColumnName("h1140");

                entity.Property(e => e.H1330).HasColumnName("h1330");

                entity.Property(e => e.H1430).HasColumnName("h1430");

                entity.Property(e => e.H1540).HasColumnName("h1540");

                entity.Property(e => e.H1650).HasColumnName("h1650");

                entity.Property(e => e.H1750).HasColumnName("h1750");

                entity.Property(e => e.H1915).HasColumnName("h1915");

                entity.Property(e => e.H2015).HasColumnName("h2015");

                entity.Property(e => e.H2125).HasColumnName("h2125");

                entity.Property(e => e.H2225).HasColumnName("h2225");

                entity.HasOne(d => d.IdFormadorNavigation)
                    .WithMany(p => p.Disponibilidade)
                    .HasForeignKey(d => d.IdFormador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__disponibi__id_fo__7993056A");
            });

            modelBuilder.Entity<Formador>(entity =>
            {
                entity.ToTable("formador");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.IsAdmin)
                    .HasColumnName("is_admin")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<FormadorModulo>(entity =>
            {
                entity.HasKey(e => new { e.IdFormador, e.IdModulo, e.IdTurma });

                entity.ToTable("formador_modulo");

                entity.Property(e => e.IdFormador).HasColumnName("id_formador");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.IdTurma).HasColumnName("id_turma");

                entity.HasOne(d => d.IdFormadorNavigation)
                    .WithMany(p => p.FormadorModulo)
                    .HasForeignKey(d => d.IdFormador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__formador___id_fo__07E124C1");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.FormadorModulo)
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__formador___id_mo__08D548FA");

                entity.HasOne(d => d.IdTurmaNavigation)
                    .WithMany(p => p.FormadorModulo)
                    .HasForeignKey(d => d.IdTurma)
                    .HasConstraintName("FK__formador___id_tu__09C96D33");
            });

            modelBuilder.Entity<Grupo>(entity =>
            {
                entity.ToTable("grupo");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdCurso).HasColumnName("id_curso");

                entity.Property(e => e.Nome)
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.Property(e => e.Ordem).HasColumnName("ordem");

                entity.HasOne(d => d.IdCursoNavigation)
                    .WithMany(p => p.Grupo)
                    .HasForeignKey(d => d.IdCurso)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK__grupo__id_curso__153B1FDF");
            });

            modelBuilder.Entity<HorarioTurmas>(entity =>
            {
                entity.HasKey(e => e.Data);

                entity.ToTable("horario_turmas");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("date");

                entity.Property(e => e.IdHora).HasColumnName("id_hora");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.IdTurma).HasColumnName("id_turma");

                entity.HasOne(d => d.IdHoraNavigation)
                    .WithMany(p => p.HorarioTurmas)
                    .HasForeignKey(d => d.IdHora)
                    .HasConstraintName("FK__horario_t__id_ho__0D99FE17");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.HorarioTurmas)
                    .HasForeignKey(d => d.IdModulo)
                    .HasConstraintName("FK__horario_t__id_mo__0CA5D9DE");

                entity.HasOne(d => d.IdTurmaNavigation)
                    .WithMany(p => p.HorarioTurmas)
                    .HasForeignKey(d => d.IdTurma)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK__horario_t__id_tu__0E8E2250");
            });

            modelBuilder.Entity<Horas>(entity =>
            {
                entity.ToTable("horas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Hora)
                    .HasColumnName("hora")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<HorasRegime>(entity =>
            {
                entity.HasKey(e => new { e.IdHora, e.IdRegime });

                entity.ToTable("horas_regime");

                entity.Property(e => e.IdHora).HasColumnName("id_hora");

                entity.Property(e => e.IdRegime).HasColumnName("id_regime");

                entity.HasOne(d => d.IdHoraNavigation)
                    .WithMany(p => p.HorasRegime)
                    .HasForeignKey(d => d.IdHora)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__horas_reg__id_ho__116A8EFB");

                entity.HasOne(d => d.IdRegimeNavigation)
                    .WithMany(p => p.HorasRegime)
                    .HasForeignKey(d => d.IdRegime)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__horas_reg__id_re__125EB334");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.CodModulo);

                entity.ToTable("modulo");

                entity.Property(e => e.CodModulo)
                    .HasColumnName("cod_modulo")
                    .ValueGeneratedNever();

                entity.Property(e => e.NmrHoras).HasColumnName("nmr_horas");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.Property(e => e.Observacoes)
                    .HasColumnName("observacoes")
                    .HasMaxLength(255);

                entity.Property(e => e.Sigla)
                    .IsRequired()
                    .HasColumnName("sigla")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<OrdemModCat>(entity =>
            {
                entity.HasKey(e => new { e.IdModulo, e.IdCategoria });

                entity.ToTable("ordem_mod_cat");

                entity.Property(e => e.IdModulo).HasColumnName("id_modulo");

                entity.Property(e => e.IdCategoria).HasColumnName("id_categoria");

                entity.Property(e => e.Ordem).HasColumnName("ordem");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.OrdemModCat)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("FK__ordem_mod__id_ca__1BE81D6E");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.OrdemModCat)
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ordem_mod__id_mo__1AF3F935");
            });

            modelBuilder.Entity<Regime>(entity =>
            {
                entity.ToTable("regime");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NomeRegime)
                    .HasColumnName("regime")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Turma>(entity =>
            {
                entity.ToTable("turma");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Coordenador)
                    .IsRequired()
                    .HasColumnName("coordenador")
                    .HasMaxLength(255);

                entity.Property(e => e.DataInicio)
                    .HasColumnName("data_inicio")
                    .HasColumnType("date");

                entity.Property(e => e.MesFerias)
                .IsRequired()
                .HasColumnName("mes_ferias");

                entity.Property(e => e.IdCurso).HasColumnName("id_curso");

                entity.Property(e => e.IdRegime).HasColumnName("id_regime");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(255);

                entity.HasOne(d => d.IdCursoNavigation)
                    .WithMany(p => p.Turma)
                    .HasForeignKey(d => d.IdCurso)
                    .HasConstraintName("FK__turma__id_curso__0504B816");

                entity.HasOne(d => d.IdRegimeNavigation)
                    .WithMany(p => p.Turma)
                    .HasForeignKey(d => d.IdRegime)
                    .HasConstraintName("FK__turma__id_regime__041093DD");
            });
        }
    }
}
