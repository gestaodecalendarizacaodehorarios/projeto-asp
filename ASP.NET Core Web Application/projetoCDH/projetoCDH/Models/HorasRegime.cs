﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class HorasRegime
    {
        public int IdHora { get; set; }
        public int IdRegime { get; set; }

        public Horas IdHoraNavigation { get; set; }
        public Regime IdRegimeNavigation { get; set; }
    }
}
