﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class Grupo
    {
        public Grupo()
        {
            Categoria = new HashSet<Categoria>();
        }

        public int Id { get; set; }
        public int? IdCurso { get; set; }
        public string Nome { get; set; }
        public int Ordem { get; set; }

        public Curso IdCursoNavigation { get; set; }
        public ICollection<Categoria> Categoria { get; set; }
    }
}
