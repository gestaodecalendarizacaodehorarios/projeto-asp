﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class Formador
    {
        public Formador()
        {
            Disponibilidade = new HashSet<Disponibilidade>();
            FormadorModulo = new HashSet<FormadorModulo>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool? IsAdmin { get; set; }

        public ICollection<Disponibilidade> Disponibilidade { get; set; }
        public ICollection<FormadorModulo> FormadorModulo { get; set; }
        public ICollection<Notificacao> Notificacao { get; set; }
    }
}
