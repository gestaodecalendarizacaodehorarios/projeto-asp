﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.Models
{
    public class Notificacao
    {
        public int Id { get; set; }
        public int IdFormador { get; set; }
        public string Turma { get; set; }
        public string PathExcel { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime HoraCriada { get; set; }
        public bool Visualizacao { get; set; }
        public bool Inserido { get; set; }
        public string PathExcelInserido { get; set; }

        public Formador IdFormadorNavigation { get; set; }
    }
}
