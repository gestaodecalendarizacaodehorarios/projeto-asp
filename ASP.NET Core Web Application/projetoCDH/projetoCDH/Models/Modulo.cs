﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class Modulo
    {
        public Modulo()
        {
            FormadorModulo = new HashSet<FormadorModulo>();
            HorarioTurmas = new HashSet<HorarioTurmas>();
            OrdemModCat = new HashSet<OrdemModCat>();
        }

        public int CodModulo { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int NmrHoras { get; set; }
        public string Observacoes { get; set; }

        public ICollection<FormadorModulo> FormadorModulo { get; set; }
        public ICollection<HorarioTurmas> HorarioTurmas { get; set; }
        public ICollection<OrdemModCat> OrdemModCat { get; set; }
    }
}
