﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class HorarioTurmas
    {
        public DateTime Data { get; set; }
        public int? IdModulo { get; set; }
        public int? IdHora { get; set; }
        public int? IdTurma { get; set; }

        public Horas IdHoraNavigation { get; set; }
        public Modulo IdModuloNavigation { get; set; }
        public Turma IdTurmaNavigation { get; set; }
    }
}
