﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class Curso
    {
        public Curso()
        {
            Grupo = new HashSet<Grupo>();
            Turma = new HashSet<Turma>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int TotalHoras { get; set; }

        public ICollection<Grupo> Grupo { get; set; }
        public ICollection<Turma> Turma { get; set; }
    }
}
