﻿using System;
using System.Collections.Generic;

namespace projetoCDH.Models
{
    public partial class OrdemModCat
    {
        public int IdModulo { get; set; }
        public int IdCategoria { get; set; }
        public int Ordem { get; set; }

        public Categoria IdCategoriaNavigation { get; set; }
        public Modulo IdModuloNavigation { get; set; }
    }
}
