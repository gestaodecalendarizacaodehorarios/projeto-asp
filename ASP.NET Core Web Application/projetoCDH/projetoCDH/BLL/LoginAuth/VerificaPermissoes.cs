﻿using Microsoft.AspNetCore.Mvc;
using projetoCDH.Models;
using projetoCDH.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.BLL.LoginAuth
{
    public class VerificaPermissoes : Controller
    {
        public ActionResult VerificaSeTaLogado(Someone sm)
        {
            if ((sm.Status ?? false) == true)
                return View("~/Views/MainPage/Index.cshtml", new ViewForModel(sm));
            else
                return RedirectToAction("IndexLogin", "Login");
        }
        public ActionResult VerificaSeTaLogadoESeEAdministrador(Someone sm)
        {
            if ((sm.Status ?? false) == true && sm.Formador.IsAdmin == true)
                return View("~/Views/MainPage/Index.cshtml", new ViewForModel(sm));
            else
                return RedirectToAction("IndexLogin", "Login");
        }
    }
}
