﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using projetoCDH.Controllers;
using projetoCDH.Models;
using projetoCDH.ViewModel;
using System;
using System.Threading.Tasks;

namespace projetoCDH.BLL.LoginAuth
{
    public class AutenticacaoDeLogin<T, U> : LoginController
    {
        private Func<T, U, bool> functionCompare;


        public AutenticacaoDeLogin(Func<T, U, bool> func)
        {
            this.functionCompare = func;
        }

        public bool AutenticarUsuario(T formador, U loginResponse, Someone sm)
        {

            if (functionCompare(formador, loginResponse))
            {
                sm.Formador = (formador as Formador);
                return true;
            }
            else
            {
                ++sm.ContaTentativas;
                return false;
            }
        }
    }
    public class Someone /*: Controller*/ /*LoginController*/
    {
        public int ContaTentativas { get; set; } = 0;
        public Formador Formador { get; set; }
        public bool? Status 
        {
            get;
            set;
        }
        public bool TimeWaiting { get; set; } = false;

        public Someone(int contTent)
        {
            ContaTentativas = contTent;
        }

        public Someone()
        {
        }
    }
    public static class SomeoneNotFound
    {
        public static Someone Someone { get; set; } = new Someone { Formador = new Formador { Username = "404", Password = "404", IsAdmin = false }, Status = false };
    }
}
