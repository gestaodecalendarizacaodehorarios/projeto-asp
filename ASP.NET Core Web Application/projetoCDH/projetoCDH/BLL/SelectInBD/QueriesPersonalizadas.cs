﻿using projetoCDH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.BLL.SelectInBD
{
    public class QueriesPersonalizadas
    {
        public class FormadorECurso
        {
            public int Id { get; set; }
            public string Nome { get; set; }
            public string Sigla { get; set; }
        }
        public static List<FormadorECurso> RetornaNomeESiglaDoCursoDoFormador()
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                var Filtrado = (from form in mcdh?.Formador
                                  join fm in mcdh?.FormadorModulo on form.Id equals fm.IdFormador
                                  join t in mcdh?.Turma on fm.IdTurma equals t.Id
                                  join c in mcdh?.Curso on t.IdCurso equals c.Id
                                  select new
                                  {
                                      Id = form.Id,
                                      Nome = form.Nome,
                                      Sigla = c.Sigla
                                  });

                List<FormadorECurso> fmecs = new List<FormadorECurso>();

                foreach (var item in Filtrado)
                {
                    fmecs.Add(new FormadorECurso
                    {
                        Id = item.Id,
                        Nome = item.Nome,
                        Sigla = item.Sigla
                    });
                }

                //var resultado3 = from formFilt in fmecs
                //                 group formFilt by formFilt.Sigla into newGroup
                //                 orderby newGroup.Key
                //                 select newGroup;

                //foreach (var nameGroup in resultado3)
                //{
                //    Console.WriteLine($"Grupo: {nameGroup.Key}");
                //    foreach (var formador in nameGroup)
                //    {
                //        Console.WriteLine($"\t{formador.Nome}");
                //    }
                //}
                #region Area de testes

                //FormadorECurso[] sd = new FormadorECurso[]
                //{

                //new FormadorECurso{
                //Nome = "Teste 1", Sigla="T1"},
                //new FormadorECurso
                //{
                //    Nome= "Teste 2", Sigla="T2"
                //},
                //new FormadorECurso
                //{
                //    Nome= "Teste 3", Sigla="T2"
                //},
                //new FormadorECurso
                //{
                //    Nome= "Teste 4", Sigla="T1"
                //},
                //                new FormadorECurso
                //{
                //    Nome= "Teste 5", Sigla="T1"
                //},
                //};

                //var result = sd.GroupBy(x => x.Sigla)
                //    .Select(x => x.ToList())
                //    .ToList();

                //foreach (var item in result)
                //{
                //    Console.WriteLine("Grupo: " + item[0].Sigla);
                //    for (int i = 0; i < item.Count; i++)
                //    {
                //        //Console.WriteLine(item[0, i].Sigla);

                //        Console.WriteLine(item[i].Nome);
                //    }
                //}

                #endregion
                return fmecs;
            }
        }
    }
}
