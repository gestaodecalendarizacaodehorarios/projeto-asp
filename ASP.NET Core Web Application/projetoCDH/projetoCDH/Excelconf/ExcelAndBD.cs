﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using OfficeOpenXml;
using projetoCDH.BLL.LoginAuth;
using projetoCDH.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.Excelconf
{
    public class ExcelAndBD
    {
        public static void MandaDisponibilidadeParaABaseDeDados(string pathExcel, Someone FromJsonSomeone)
        {
            using (FileStream fs = File.OpenRead(pathExcel))
            using (ExcelPackage excelPackage = new ExcelPackage(fs))
            {
                ExcelWorkbook xlsWorkBook = excelPackage.Workbook;
                ExcelWorksheet xlsWorkSheet = xlsWorkBook.Worksheets.First();
                string nomeTurma = xlsWorkSheet.Name;

                var colorOcupado = xlsWorkSheet.Cells[8, 3].Style.Fill.BackgroundColor.Rgb;
                var colorDisponivel = xlsWorkSheet.Cells[9, 3].Style.Fill.BackgroundColor.Rgb;
                var colorCinza = xlsWorkSheet.Cells[3, 3].Style.Fill.BackgroundColor.Rgb;

                try
                {
                    var smt = xlsWorkSheet.Cells[10, 1].Value;
                    var lastColumnRowOne = xlsWorkSheet.Dimension.End.Column;

                    var qtala = xlsWorkSheet.Cells[1, lastColumnRowOne - 1];

                    using (Models.ModelCDHContext mcdh = new Models.ModelCDHContext())
                    {

                        Models.Turma tma = mcdh.Turma.FirstOrDefault(x => x.Nome == nomeTurma);
                        Models.Regime regm = mcdh.Regime.FirstOrDefault(x => x.Id == tma.IdRegime);

                        bool H0830;
                        bool H0930;
                        bool H1040;
                        bool H1140;
                        bool H1330;
                        bool H1430;
                        bool H1540;
                        bool H1650;
                        bool H1750;
                        bool H1915;
                        bool H2015;
                        bool H2125;
                        bool H2225;


                        if (regm.NomeRegime == "Diurno")
                        {
                            for (int i = 3; i < lastColumnRowOne; i++)
                            {

                                if (xlsWorkSheet.Cells[3, i].Style.Fill.BackgroundColor.Rgb == colorDisponivel)
                                {
                                    H0830 = true;
                                    H0930 = true;
                                    H1040 = true;
                                    H1140 = true;
                                }
                                else
                                {
                                    H0830 = false;
                                    H0930 = false;
                                    H1040 = false;
                                    H1140 = false;
                                }
                                if (xlsWorkSheet.Cells[4, i].Style.Fill.BackgroundColor.Rgb == colorDisponivel)
                                {
                                    H1330 = true;
                                    H1430 = true;
                                    H1540 = true;
                                }
                                else
                                {
                                    H1330 = false;
                                    H1430 = false;
                                    H1540 = false;
                                }


                                mcdh.Disponibilidade.Add(new Models.Disponibilidade
                                {
                                    IdFormador = FromJsonSomeone.Formador.Id,
                                    Data = DateTime.Parse(xlsWorkSheet.Cells[1, i].Text),
                                    H0830 = H0830,
                                    H0930 = H0930,
                                    H1040 = H1040,
                                    H1140 = H1140,

                                    H1330 = H1330,
                                    H1430 = H1430,
                                    H1540 = H1540,

                                    H1650 = false,
                                    H1750 = false,
                                    H1915 = false,
                                    H2015 = false,
                                    H2125 = false,
                                    H2225 = false,
                                });
                            }
                        }
                        if (regm.NomeRegime == "Tarde")
                        {
                            for (int i = 3; i < lastColumnRowOne; i++)
                            {
                                if (xlsWorkSheet.Cells[5, i].Style.Fill.BackgroundColor.Rgb == colorDisponivel)
                                {

                                    H1650 = true;
                                    H1750 = true;

                                }
                                else
                                {
                                    H1650 = false;
                                    H1750 = false;
                                }
                                if (xlsWorkSheet.Cells[6, i].Style.Fill.BackgroundColor.Rgb == colorDisponivel)
                                {
                                    H1915 = true;
                                    H2015 = true;
                                    H2125 = true;
                                    H2225 = true;
                                }
                                else
                                {
                                    H1915 = false;
                                    H2015 = false;
                                    H2125 = false;
                                    H2225 = false;
                                }
                                mcdh.Disponibilidade.Add(new Models.Disponibilidade
                                {
                                    IdFormador = FromJsonSomeone.Formador.Id,
                                    Data = DateTime.Parse(xlsWorkSheet.Cells[1, i].Text),

                                    H0830 = false,
                                    H0930 = false,
                                    H1040 = false,
                                    H1140 = false,
                                    H1330 = false,
                                    H1430 = false,
                                    H1540 = false,

                                    H1650 = H1650,
                                    H1750 = H1750,
                                    H1915 = H1915,
                                    H2015 = H2015,
                                    H2125 = H2125,
                                    H2225 = H2225,
                                });
                            }
                        }
                        if (regm.NomeRegime == "Pós-laboral")
                        {
                            for (int i = 3; i < lastColumnRowOne; i++)
                            {
                                if (xlsWorkSheet.Cells[6, i].Style.Fill.BackgroundColor.Rgb == colorDisponivel)
                                {
                                    H1915 = true;
                                    H2015 = true;
                                    H2125 = true;
                                    H2225 = true;
                                }
                                else
                                {
                                    H1915 = false;
                                    H2015 = false;
                                    H2125 = false;
                                    H2225 = false;
                                }
                                mcdh.Disponibilidade.Add(new Models.Disponibilidade
                                {
                                    Data = DateTime.Parse(xlsWorkSheet.Cells[1, i].Text),

                                    IdFormador = FromJsonSomeone.Formador.Id,

                                    H0830 = false,
                                    H0930 = false,
                                    H1040 = false,
                                    H1140 = false,
                                    H1330 = false,
                                    H1430 = false,
                                    H1540 = false,
                                    H1650 = false,
                                    H1750 = false,

                                    H1915 = H1915,
                                    H2015 = H2015,
                                    H2125 = H2125,
                                    H2225 = H2225,

                                });
                            }
                        }

                        mcdh.SaveChanges();

                    }


                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish

                }
                catch (Exception e)
                {
                    GC.Collect(); // Start .NET CLR Garbage Collection
                    GC.WaitForPendingFinalizers(); // Wait for Garbage Collection to finish

                    throw e;
                }
            }
        }
    }
}
