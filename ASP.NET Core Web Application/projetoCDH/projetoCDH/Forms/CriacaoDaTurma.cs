﻿//using projetoCDH.Models;
using projetoCDH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.Forms
{
    public class CriacaoDaTurma
    {
        public string Nome { get; set; }
        public Regime Regime { get; set; }
        public Curso Curso { get; set; }
        public string DataDeInicio { get; set; }
        public Formador  Coordenador { get; set; }
        public List<Formador> FormadoresDaTurma { get; set; }
    }
}
