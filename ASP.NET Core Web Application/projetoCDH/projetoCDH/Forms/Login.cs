﻿using projetoCDH.BLL.LoginAuth;

namespace projetoCDH.Forms
{
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
