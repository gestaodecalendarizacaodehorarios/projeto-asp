﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.Forms
{
    public class UploadFileExcel
    {
        public string IdNotificacao { get; set; }
        public string NomeTurma { get; set; }
        public IFormFile FileExcel { get; set; }

    }
}
