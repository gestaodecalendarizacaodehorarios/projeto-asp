﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using projetoCDH.BLL.LoginAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace projetoCDH.Controllers
{
    public class HorarioController : Controller
    {
        public Someone FromJsonSomeone
        {
            get => HttpContext.Session.GetString("objJSONUsuario") == null ? SomeoneNotFound.Someone : JsonConvert.DeserializeObject<Someone>(HttpContext.Session.GetString("objJSONUsuario"));
        }
        [HttpGet("/planofuncionamento")]
        public ActionResult PlanoFuncView()
        {
            return View(new projetoCDH.ViewModel.ViewForModel(FromJsonSomeone));
            //return View("PlanoFuncView");
        }

    }
}
