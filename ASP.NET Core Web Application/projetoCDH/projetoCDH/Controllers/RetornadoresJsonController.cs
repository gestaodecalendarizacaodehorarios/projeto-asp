﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
//using Horarios.ExcelHorario;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using projetoCDH.Models;
using projetoCDH.ExcelCalendarioProvisorio;
using System.IO;
using OfficeOpenXml;

namespace projetoCDH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RetornadoresJsonController : ControllerBase
    {
        public static IEnumerable<Formador> FormadoresPQuery { get; set; }
        public static IEnumerable<Turma> TurmasPQuery { get; set; }
        public static IEnumerable<Curso> CursosPQuery { get; set; }
        public static IEnumerable<Modulo> ModulosPQuery { get; set; }
        public static IEnumerable<Categoria> CategoriasPQuery { get; set; }
        public static IEnumerable<Grupo> GruposPQuery { get; set; }


        #region CRUD Utilizador

        [HttpGet("/RetornaJSONformadores")]
        public ActionResult RetornaJSONformadores()
        {
            List<object> objAnonimo = new List<object>();

            foreach (Formador fmd in FormadoresPQuery)
            {
                objAnonimo.Add(new
                {
                    Id = fmd.Id,
                    Nome = fmd.Nome,
                    Email = fmd.Email,
                    Username = fmd.Username,
                    Password = fmd.Password,
                    IsAdmin = (fmd.IsAdmin == true) ? "Sim" : "Não",
                    Actions = $"<button type=\"button\" class=\"btn btn-outline-primary\" data-toggle=\"modal\" style=\"width:73px\" data-target=\"#ModalEdit\">Editar</button> <button type=\"button\" class=\"btn btn-outline-danger\"  onclick=\"deleterow({fmd.Id});\">Deletar</button>"
                });
            }
            return new JsonResult(objAnonimo);
        }

        [Route("/InsereUtilizador")]
        public ActionResult InsereUtilizador([FromBody]Formador form)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                mcdh.Formador.Add(new Formador
                {
                    Nome = form.Nome,
                    Username = form.Username,
                    Password = form.Password,
                    Email = form.Email,
                    IsAdmin = form.IsAdmin
                });
                mcdh.SaveChanges();
            }
            return new JsonResult("success");
        }

        [Route("/EditaUtilizador")]
        public IActionResult EditaUtilizador([FromBody]Formador docente)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Formador formador = (from f1 in FormadoresPQuery
                                     where f1.Id == docente.Id
                                     select f1).FirstOrDefault();
                formador.Nome = docente.Nome;
                formador.Username = docente.Username;
                formador.Password = docente.Password;
                formador.Email = docente.Email;
                formador.IsAdmin = docente.IsAdmin;

                mcdh.SaveChanges();
            }
            return new JsonResult("success");
        }

        [Route("/DeletaFormador")]
        public ActionResult DeletaFormador([FromBody]Formador formador)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Formador fil = (from f1 in mcdh.Formador where f1.Id == formador.Id select f1).First();
                //delete it
                mcdh.Formador.Remove(fil);
                //save it
                mcdh.SaveChanges();
                FormadoresPQuery = mcdh.Formador.ToList().Where(x => x.Id != 1);
            }

            return new JsonResult("success");
        }



        #region Retornadores true e false
        [Route("/RetornaUsernameTrueSeIgual")]
        public ActionResult RetornaUsernameTrueSeIgual([FromBody] Formador objCNome)
        {
            Formador fm = new Formador();
            if (objCNome != null)
                fm = FormadoresPQuery.FirstOrDefault(formador => formador.Username == objCNome.Username) ?? null;
            bool result = (fm != null) ? true : false;

            return new JsonResult(result);
        }
        [Route("/RetornaEmailTrueSeIgual")]
        public ActionResult RetornaEmailTrueSeIgual([FromBody]Formador objCNome)
        {
            Formador fm = new Formador();
            if (objCNome != null)
                fm = FormadoresPQuery.FirstOrDefault(formador => formador.Email == objCNome.Email) ?? null;
            bool result = (fm != null) ? true : false;

            return new JsonResult(result);
        }
        [Route("/RetornaTrueSeNomeIgual")]
        public ActionResult RetornaTrueSeNomeIgual([FromBody] Formador formador)
        {
            Formador fm = new Formador();
            if (formador != null)
                fm = FormadoresPQuery.FirstOrDefault(fmd => fmd.Nome == formador.Nome) ?? null;
            bool result = (fm != null) ? true : false;

            return new JsonResult(result);
        }
        #endregion


        #endregion
       
        #region CRUD Turma

        [Route("/InserirTurma")]
        public ActionResult InsereTurma([FromBody] Turma turma)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Turma objTurma = new Turma
                {
                    IdRegime = turma.IdRegime,
                    IdCurso = turma.IdCurso,
                    Nome = turma.Nome,
                    //DataInicio = DateTime.Parse(turma.DataInicio.ToString("dd/M/yyyy")),
                    DataInicio = DateTime.Parse(turma.DataInicio.ToString()),
                    MesFerias = turma.MesFerias,
                    Coordenador = turma.Coordenador
                };
                mcdh.Turma.Add(objTurma);
                mcdh.SaveChanges();
                return new JsonResult(objTurma.Id);
            }
        }

        [Route("/AssociarFormadores")]
        public ActionResult AssociaFormadores([FromBody] FormadorModulo[] formModulo)
        {
            if (formModulo != null)
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    foreach (FormadorModulo item in formModulo)
                    {
                        mcdh.Add(new FormadorModulo
                        {
                            IdFormador = item.IdFormador,
                            IdModulo = item.IdModulo,
                            IdTurma = item.IdTurma
                        });
                    }
                    mcdh.SaveChanges();
                    CriaExceisDeUmaTurmaParaOsFormadores(formModulo);
                    return new JsonResult("success");
                }
            return new JsonResult("fail");
        }
        #region CriaExceis
        public void CriaExceisDeUmaTurmaParaOsFormadores(FormadorModulo[] formModulo)
        {
            //List<int> idFormadores = formModulo[0].IdFormador;
            Turma turmaPHorario;
            List<Formador> listaDeFormador = new List<Formador>();
            string resultTotalHora;
            string nomeRegime = "";
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                turmaPHorario = mcdh.Turma.FirstOrDefault(x => x.Id == formModulo[0].IdTurma);
                FormadoresPQuery = mcdh.Formador.ToList();

                foreach (var item in formModulo)
                    if (FormadoresPQuery.FirstOrDefault(x => x.Id == item.IdFormador) != null)
                        listaDeFormador.Add(FormadoresPQuery.FirstOrDefault(x => x.Id == item.IdFormador));

                resultTotalHora = (from cur in mcdh.Curso
                             join tur in mcdh.Turma
                             on cur.Id equals tur.IdCurso
                             where tur.Id == formModulo[0].IdTurma
                             select cur.TotalHoras
                             ).First().ToString();

                nomeRegime = (from reg in mcdh.Regime
                                    join tur in mcdh.Turma
                                    on reg.Id equals tur.IdRegime
                                    where turmaPHorario.IdRegime == reg.Id
                                    select reg.NomeRegime
                                    ).First().ToString();
            }
            if(nomeRegime == "Diurno")
            {
                InfoParaUmHorario infUmHorario = new InfoParaUmHorario();

                string pathDiurno = infUmHorario.HorarioDiurno(new InfoParaUmHorario
                {
                    DataDeInicio = turmaPHorario.DataInicio,
                    NmrTotalDeHorasCurso = int.Parse(resultTotalHora),
                    MesDeFerias = turmaPHorario.MesFerias,
                    NomeDaTurma = turmaPHorario.Nome,
                    RegimeTurma = nomeRegime
                });
                infUmHorario.CriaHorarioParaOsFormadoresDiurnos(listaDeFormador.ToArray(), pathDiurno, turmaPHorario.Nome);



            }
            else if (nomeRegime == "Tarde")
            {
                InfoParaUmHorario infUmHorario = new InfoParaUmHorario();

                string pathTarde = infUmHorario.HorarioDaTarde(new InfoParaUmHorario
                {
                    DataDeInicio = turmaPHorario.DataInicio,
                    NmrTotalDeHorasCurso = int.Parse(resultTotalHora),
                    MesDeFerias = turmaPHorario.MesFerias,
                    NomeDaTurma = turmaPHorario.Nome,
                    RegimeTurma = nomeRegime
                });
                infUmHorario.CriaHorarioParaOsFormadoresDaTarde(listaDeFormador.ToArray(), pathTarde, turmaPHorario.Nome);
            }
            else if (nomeRegime == "Pós-laboral")
            {
                InfoParaUmHorario infUmHorario = new InfoParaUmHorario();

                string pathPLaboral = infUmHorario.HorarioPosLaboral(new InfoParaUmHorario
                {
                    DataDeInicio = turmaPHorario.DataInicio,
                    NmrTotalDeHorasCurso = int.Parse(resultTotalHora),
                    MesDeFerias = turmaPHorario.MesFerias,
                    NomeDaTurma = turmaPHorario.Nome,
                    RegimeTurma = nomeRegime
                });
                infUmHorario.CriaHorarioParaOsPosLaborais(listaDeFormador.ToArray(), pathPLaboral, turmaPHorario.Nome);
            }

        }
        #endregion


        #region Retornadores


        public class NomeTurma
        {
            public string NomeDaTuma { get; set; }
        }
        [Route("/RetornaTrueSeTurmaNomeIgual")]
        public ActionResult RetornaTrueSeTurmaNomeIgual([FromBody] NomeTurma nomeTurma)
        {
            Turma tm = TurmasPQuery.FirstOrDefault(x => x.Nome == nomeTurma.NomeDaTuma);
            if (tm != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        #endregion

        #endregion

        #region CRUD Curso
        [Route("/InserirCurso")]
        public ActionResult InsereCurso([FromBody] Curso curso)
        {
            if (curso != null)
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    mcdh.Curso.Add(new Curso()
                    {
                        Nome = curso.Nome,
                        Sigla = curso.Sigla,
                        TotalHoras = curso.TotalHoras,
                    });
                    mcdh.SaveChanges();
                    return new JsonResult("Success");
                }
            else
                return new JsonResult("Failed");
        }




        #region Retornadores

        [Route("/RetornaTrueSeNomeCursoIgual")]
        public ActionResult RetornaTrueSeNomeCursoIgual([FromBody] Curso curso)
        {
            Curso cursoResult = CursosPQuery.FirstOrDefault(x => x.Nome == curso.Nome);
            if (cursoResult != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);

        }
        #endregion

        #endregion

        #region CRUD Módulo

        [Route("/InserirModulo")]
        public ActionResult InserirModulo([FromBody] Modulo modulo)
        {
            if (modulo != null)
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    mcdh.Modulo.Add(new Modulo()
                    {
                        CodModulo = modulo.CodModulo,
                        Nome = modulo.Nome,
                        Sigla = modulo.Sigla,
                        NmrHoras = modulo.NmrHoras,
                        Observacoes = modulo.Observacoes
                    });
                    mcdh.SaveChanges();
                    return new JsonResult("Success");
                }
            else
                return new JsonResult("Failed");
        }

        [Route("/RetornaTrueSeNomeModuloIgual")]
        public ActionResult RetornaTrueSeNomeModuloIgual([FromBody] Modulo modulo)
        {
            Modulo mod = ModulosPQuery.FirstOrDefault(x => x.Nome == modulo.Nome);
            if (mod != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        [Route("/RetornaTrueSeSiglaModuloIgual")]
        public ActionResult RetornaTrueSeSiglaModuloIgual([FromBody] Modulo modulo)
        {
            Modulo mod = ModulosPQuery.FirstOrDefault(x => x.Sigla == modulo.Sigla);
            if (mod != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        #region Gerir Módulo
        [Route("/DeletaModulo")]
        public ActionResult DeletaModulo([FromBody] Modulo modulo)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Modulo mod = (from f1 in mcdh.Modulo where f1.CodModulo == modulo.CodModulo select f1).First();
                //delete it
                mcdh.Modulo.Remove(mod);
                //save it
                mcdh.SaveChanges();
                ModulosPQuery = mcdh.Modulo.ToList();
            }
            return new JsonResult("success");
        }

        [Route("/EditaModulo")]
        public ActionResult EditaModulo([FromBody] Modulo modulo)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Modulo mod = (from f1 in ModulosPQuery
                                     where f1.CodModulo == modulo.CodModulo
                                     select f1).FirstOrDefault();

                mod.CodModulo = modulo.CodModulo;
                mod.Nome = modulo.Nome;
                mod.Sigla = modulo.Sigla;
                mod.NmrHoras = modulo.NmrHoras;
                mod.Observacoes = modulo.Observacoes;

                mcdh.SaveChanges();
            }
            return new JsonResult("success");
        }

        [Route("/RetornaJSONmodulos")]
        public ActionResult RetornaJSONmodulos()
        {
            List<object> objAnonimo = new List<object>();
            IEnumerable<Modulo> modulos;

            modulos = ModulosPQuery.ToList();
            foreach (Modulo cod in modulos)
            {
                objAnonimo.Add(new
                {
                    CodModulo = cod.CodModulo,
                    Nome = cod.Nome,
                    Sigla = cod.Sigla,
                    NmrHoras = cod.NmrHoras,
                    Observacoes = cod.Observacoes,
                    Actions = $"<button type=\"button\" class=\"btn btn-outline-primary\" data-toggle=\"modal\" style=\"width:73px\" data-target=\"#ModalEdit\">Editar</button> <button type=\"button\" class=\"btn btn-outline-danger\"  onclick=\"deleterow({cod.CodModulo});\">Deletar</button>"
                });
            }
            return new JsonResult(objAnonimo);
        }

        [Route("/RetornaTrueSeNomeCodigoModuloIgual")]
        public ActionResult RetornaTrueSeNomeCodigoModuloIgual([FromBody] Modulo modulo)
        {
            Modulo mod = ModulosPQuery.FirstOrDefault(x => x.CodModulo == modulo.CodModulo);
            if (mod != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        [Route("/RetornaTrueSeObservacoesModuloIgual")]
        public ActionResult RetornaTrueSeObservacoesModuloIgual([FromBody] Modulo modulo)
        {
            Modulo mod = ModulosPQuery.FirstOrDefault(x => x.Observacoes == modulo.Observacoes);
            if (mod != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        #endregion





        #endregion

        #region CRUD Categoria

        [Route("/InserirCategoria")]
        public ActionResult InserirCategoria([FromBody] Categoria categoria)
        {
            if (categoria != null)
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    Categoria objCategoria = new Categoria();
                    objCategoria.IdGrupo = categoria.IdGrupo;
                    objCategoria.Nome = categoria.Nome;
                    objCategoria.Ordem = categoria.Ordem;

                    mcdh.Add(objCategoria);
                    mcdh.SaveChanges();

                    return new JsonResult(objCategoria.Id);
                }
            else
                return new JsonResult(false);
        }

        [Route("/RelacionaModuloECategoria")]
        public ActionResult RelacionaModuloECategoria([FromBody] OrdemModCat[] formadorModulos)
        {
            if (formadorModulos != null)
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    foreach (var item in formadorModulos)
                    {
                        mcdh.Add(new OrdemModCat
                        {
                            IdModulo = item.IdModulo,
                            IdCategoria = item.IdCategoria,
                            Ordem = item.Ordem
                        });
                    }

                    mcdh.SaveChanges();
                    return new JsonResult(true);
                }
            else
                return new JsonResult(false);
        }

        [Route("/RetornaTrueSeCategoriaNomeIgual")]
        public ActionResult RetornaTrueSeCategoriaNomeIgual([FromBody] Categoria categoria)
        {
            Categoria cat = CategoriasPQuery.FirstOrDefault(x => x.Nome == categoria.Nome);
            if (cat != null)
                return new JsonResult(true);
            else
                return new JsonResult(false);
        }

        #endregion

        #region Baixa Excel
        
        public class ValidaDisponibilidade
        {
            public int IdNotificacao { get; set; }
            public string LinkDownload { get; set; }
        }

        [Route("/ValidaNotificacoes")]
        public ActionResult ValidaNotificacoes([FromBody] ValidaDisponibilidade bc)
        {
            Notificacao nt = new Notificacao();
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                nt = mcdh.Notificacao.FirstOrDefault(x => x.Id == bc.IdNotificacao);
                nt.Visualizacao = false;
                mcdh.SaveChanges();
            }

            return new JsonResult(bc.LinkDownload);
        }

        [Route("/Teste")]
        public string Teste(string str)
        {
            return "oi" + str;
        }

        [Route("/DownloadExcel")]
        public ActionResult DownloadExcel(string path, string idNotificacao)
        {
            Notificacao ntf = new Notificacao();

            using (ModelCDHContext mcdh = new ModelCDHContext())
                ntf = mcdh.Notificacao.FirstOrDefault(x => x.Id == int.Parse(idNotificacao));

            using (FileStream fs = System.IO.File.OpenRead(path))
            using (ExcelPackage excelPackage = new ExcelPackage(fs))
            {
                var FileBytesArray = excelPackage.GetAsByteArray();
                return File(FileBytesArray, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"{ntf.Turma}_Horario_"+ Guid.NewGuid() + ".xlsx");
            }
        }

        #endregion

    }
}