﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using projetoCDH.BLL.LoginAuth;
using projetoCDH.Excelconf;
using projetoCDH.Forms;
using projetoCDH.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace projetoCDH.Controllers
{
    public class RetornadoresJSONFormadorController : Controller
    {
        public Someone FromJsonSomeone
        {
            get => HttpContext.Session.GetString("objJSONUsuario") == null ? SomeoneNotFound.Someone : JsonConvert.DeserializeObject<Someone>(HttpContext.Session.GetString("objJSONUsuario"));
        }
        public class NaoRepeteNomeFormador : IEqualityComparer<Notificacao>
        {
            public bool Equals(Notificacao x, Notificacao y)
            {
                return x.IdFormador == y.IdFormador;
            }

            public int GetHashCode(Notificacao obj)
            {
                return obj.IdFormador.GetHashCode();
            }
        }


        public class TurmaRelacionada : IEqualityComparer<TurmaRelacionada>
        {
            public int IdNotificacao { get; set; }
            public DateTime HoraCriada { get; set; }
            public string NomeTurma { get; set; }
            public string NomeFormador { get; set; }
            public bool NotificacaoInserida { get; set; }
            public string PathExcel { get; set; }
            //pega o nome das turmas e dos formadores que estão associados a notificacao
            public static IEnumerable<TurmaRelacionada> Get(int id)
            {

                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    return (from noti in mcdh.Notificacao
                            join fm in mcdh?.Formador on noti.IdFormador equals fm.Id
                            where noti.IdFormador == id
                            select new TurmaRelacionada
                            {
                                HoraCriada = noti.HoraCriada,
                                IdNotificacao = noti.Id,
                                NomeTurma = noti.Turma,
                                NomeFormador = fm.Nome,
                                NotificacaoInserida = noti.Inserido,
                                PathExcel = noti.PathExcel
                            }).ToList().Distinct(new TurmaRelacionada());
                }
            }

            public bool Equals(TurmaRelacionada x, TurmaRelacionada y)
            {
                return x.NomeTurma == y.NomeTurma;
            }

            public int GetHashCode(TurmaRelacionada obj)
            {
                return obj.NomeTurma.GetHashCode();
            }
        }

        public static IEnumerable<Notificacao> NotificacoesPQuery { get; set; }
        public static IEnumerable<Turma> TurmasPQuery { get; set; }


        [Route("/RetornaJsonHistorico")]
        public ActionResult RetornaJsonHistorico()
        {
            List<object> objAnonimo = new List<object>();
            string makeAction = "";

            Someone sm = HttpContext.Session.GetString("objJSONUsuario") == null ? SomeoneNotFound.Someone : JsonConvert.DeserializeObject<Someone>(HttpContext.Session.GetString("objJSONUsuario"));

            IEnumerable<Notificacao> listaNotificacoes = NotificacoesPQuery.ToList().Where(x => x.IdFormador == sm.Formador.Id);

            IEnumerable<TurmaRelacionada> tmr = TurmaRelacionada.Get(sm.Formador.Id);
            foreach (TurmaRelacionada not in tmr)
            {
                Regime rgm = new Regime();
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {

                    Turma tma = mcdh.Turma.FirstOrDefault(x => x.Nome == not.NomeTurma);

                    rgm = mcdh.Regime.FirstOrDefault(x => x.Id == tma.IdRegime);
                    var tesdste = tma.IdRegimeNavigation.NomeRegime;

                }
                makeAction = (not.NotificacaoInserida) ?
                    $"<div class=\"btn btn-success btn-circle\"><i class=\"fas fa-check\"></i></div>" : $"<a class=\"btn btn-dark btn-circle\" style=\"cursor: pointer;\"onclick=\"BaixaExcel({not.IdNotificacao},\'{not.PathExcel}\')\"><i class=\"fa fa-arrow-down\" style=\"color:white\"aria-hidden=\"true\"></i></a>";

                //var s = String.Format(new System.Globalization.CultureInfo("pt-PT"), "{0:f}", not?.HoraCriada ?? DateTime.Now);
                objAnonimo.Add(new
                {
                    Id = not?.IdNotificacao ?? 0,
                    NomeTurma = not?.NomeTurma ?? "zero",
                    HoraCriada = String.Format(new System.Globalization.CultureInfo("pt-PT"), "{0:F}", not?.HoraCriada) ?? "No Time",
                    NomeRegime = rgm.NomeRegime ?? "Sem regime",
                    Status = makeAction
                });
            }
            return new JsonResult(objAnonimo);
        }

        [Route("/RetornaJsonHorarios")]
        public ActionResult RetornaJsonHorarios()
        {
            List<object> objAnonimo = new List<object>();
            string makeAction = "";

            Someone sm = HttpContext.Session.GetString("objJSONUsuario") == null ? SomeoneNotFound.Someone : JsonConvert.DeserializeObject<Someone>(HttpContext.Session.GetString("objJSONUsuario"));

            IEnumerable<Notificacao> listaNotificacoes = NotificacoesPQuery.ToList().Where(x => x.IdFormador == sm.Formador.Id);

            IEnumerable<TurmaRelacionada> tmr = TurmaRelacionada.Get(sm.Formador.Id);
            foreach (TurmaRelacionada not in tmr)
            {

                if (not.NotificacaoInserida == false)
                {
                    Regime rgm = new Regime();
                    using (ModelCDHContext mcdh = new ModelCDHContext())
                    {

                        Turma tma = mcdh.Turma.FirstOrDefault(x => x.Nome == not.NomeTurma);

                        rgm = mcdh.Regime.FirstOrDefault(x => x.Id == tma.IdRegime);
                        var tesdste = tma.IdRegimeNavigation.NomeRegime;

                    }
                    makeAction = $"<a style=\"color:white; background:#ff12a9;border-color:gray; cursor:pointer;\"" +
                        $"class=\"btn btn-primary btn-circle\" data-toggle=\"modal\" data-target=\"#uploadModal\" " +
                        $"data-idnotificacao=\"{not.IdNotificacao}\" data-nometurma=\"{not.NomeTurma}\"><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></a>";

                    //var s = String.Format(new System.Globalization.CultureInfo("pt-PT"), "{0:f}", not?.HoraCriada ?? DateTime.Now);
                    objAnonimo.Add(new
                    {
                        Id = not?.IdNotificacao ?? 0,
                        NomeTurma = not?.NomeTurma ?? "zero",
                        HoraCriada = String.Format(new System.Globalization.CultureInfo("pt-PT"), "{0:F}", not?.HoraCriada) ?? "No Time",
                        NomeRegime = rgm.NomeRegime ?? "Sem regime",
                        Status = makeAction
                    });
                }
            }
            return new JsonResult(objAnonimo);
        }

        [Route("/RetornaJSONTurma")]
        public ActionResult RetornaJSONTurma()
        {
            List<object> objAnonimo = new List<object>();

            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                List<Regime> regimes = new List<Regime>();
                List<Curso> cursos = new List<Curso>();
                IEnumerable<Turma> tmas = mcdh.Turma.ToList();
                regimes = mcdh.Regime.ToList();
                cursos = mcdh.Curso.ToList();

                foreach (var tm in tmas)
                {
                    //string situacao = $"<a style=\"color:white; background:#5a5c69; cursor:pointer;\"" +
                    //     $"class=\"btn btn-primary btn-circle\" data-toggle=\"modal\" data-target=\"#situacaoModal\" " +
                    //     $"data-idregime=\"{tm.IdRegime}\"><i class=\"fa fa-upload\" aria-hidden=\"true\"></i></a>";

                    string situacao = $"<button type=\"button\" class=\"btn btn-outline-primary\" data-idregime=\"{tm.IdRegime}\" data-idcurso=\"{tm.IdCurso}\"data-toggle=\"modal\" style=\"width:123px\" data-target=\"#situacaoModal\">Situação</button>";

                    Regime nomeRegime = regimes.FirstOrDefault(x => x.Id == tm?.IdRegime);
                    Curso nomeCurso = cursos.FirstOrDefault(x => x.Id == tm?.IdCurso);
                    objAnonimo.Add(new
                    {
                        Id = tm.Id,
                        Nome = tm.Nome,
                        NomeRegime = nomeRegime.NomeRegime,
                        NomeCurso = nomeCurso.Nome,
                        DataInicio = String.Format(new System.Globalization.CultureInfo("pt-PT"), "{0:dd/MM/yyyy}", tm?.DataInicio) ?? "No Time",
                        Coordenador = tm.Coordenador,
                        Situacao = situacao 
                    });
                }
            }
            return new JsonResult(objAnonimo);
        }

        [Route("/RetornaFormadoresQueFaltamMandarHorariosDeUmaDeterminadaTurma")]
        public ActionResult RetornaFormadoresQueFaltamMandarHorariosDeUmaDeterminadaTurma(string idTurma)
        {
            List<Formador> formadores = new List<Formador>();
            List<object> objAnonimo = new List<object>();

            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                Turma tm = mcdh.Turma.FirstOrDefault(x => x.Id == int.Parse(idTurma));
                IEnumerable<Notificacao> ntf = mcdh.Notificacao.Where(x => x.Turma == tm.Nome).ToList()/*.Distinct(new NaoRepeteNomeFormador())*/;
                List<Formador> fmdres = mcdh.Formador.ToList();

                foreach (var item in ntf)
                {
                    Formador fmd = fmdres.FirstOrDefault(x=> x.Id == item.IdFormador);
                    objAnonimo.Add(new
                    {
                        IdFormador = fmd.Id,
                        NomeFormador = fmd.Nome,
                        Username = fmd.Username,
                        Email = fmd.Email,
                        Inserido = item.Inserido
                    });
                }
                return new JsonResult(objAnonimo);
            }

        }




        [HttpPost]
        public ActionResult UploadaExcel(UploadFileExcel uploadFileExcel)
        {
            //string pathDiurno = $"{Directory.GetCurrentDirectory()}\\Formadores\\{item.Nome}\\Diurno\\";
            var ext = Path.GetExtension(uploadFileExcel.FileExcel.FileName);
            if (ext.ToLower().Equals(".xlsx") || ext.ToLower().Equals(".xls") && ext.Length < 40 * Math.Pow(1024, 2))
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    string path = "";
                    Notificacao fmd = mcdh.Notificacao.FirstOrDefault(x => x.Id == int.Parse(uploadFileExcel.IdNotificacao));
                    Turma turma = mcdh.Turma.FirstOrDefault(x => x.Nome == uploadFileExcel.NomeTurma);
                    Regime regime = mcdh.Regime.FirstOrDefault(x => x.Id == turma.IdRegime);

                    if (regime.NomeRegime == "Diurno")
                    {
                        path = $"{Directory.GetCurrentDirectory()}\\Horarios Inseridos\\Formadores\\{FromJsonSomeone.Formador.Nome}\\Diurno\\";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                    }
                    else if (regime.NomeRegime == "Tarde")
                    {
                        path = $"{Directory.GetCurrentDirectory()}\\Horarios Inseridos\\Formadores\\{FromJsonSomeone.Formador.Nome}\\Tarde\\";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                    }
                    else if (regime.NomeRegime == "Pós-laboral")
                    {
                        path = $"{Directory.GetCurrentDirectory()}\\Horarios Inseridos\\Formadores\\{FromJsonSomeone.Formador.Nome}\\Pós-laboral\\";
                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                    }
                    path = Path.Combine(path, $"_{uploadFileExcel.NomeTurma}_{Guid.NewGuid()}{ext}");

                    using (var stream = new FileStream(path, FileMode.Create))
                        uploadFileExcel.FileExcel.CopyTo(stream);

                    ExcelAndBD.MandaDisponibilidadeParaABaseDeDados(path, FromJsonSomeone);

                    fmd.Inserido = true;
                    fmd.Visualizacao = false;
                    fmd.PathExcelInserido = path.Replace("\\", "\\\\");
                    mcdh.SaveChanges();
                }
            }
            return RedirectToAction("Horarios", "MainPage");
        }

    }
}
