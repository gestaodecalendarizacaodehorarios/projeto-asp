﻿using projetoCDH.Models;
using Microsoft.AspNetCore.Mvc;
using projetoCDH.BLL.LoginAuth;
using projetoCDH.Forms;
using System.Linq;
using System.Collections.Generic;
using projetoCDH.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace projetoCDH.Controllers
{
    public class LoginController : Controller
    {
        public Someone CriaTentativasCookie(Someone sm)
        {
            if (Request.Cookies["UserCookieTentativas"] == null)
            {
                var option = new CookieOptions();
                option.Expires = DateTime.Now.AddMinutes(30);
                Response.Cookies.Append("UserCookieTentativas", sm.ContaTentativas.ToString(), option);
            }
            else { 
                sm = new Someone(int.Parse(Request.Cookies["UserCookieTentativas"]));
                if (int.Parse(Request.Cookies["UserCookieTentativas"]) >= 5)
                {
                    var option = new CookieOptions();
                    option.Expires = DateTime.Now.AddMinutes(30);

                    Task.Run(async delegate
                    {
                        await Task.Delay(6000*5);
                        Response.Cookies.Append("UserCookieTentativas", 0.ToString(), option);
                    });
                }
            }
            return sm;
        }

        public void AdicionaUmaTentativa()
        {

            var option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(30);

            int result = int.Parse(Request.Cookies["UserCookieTentativas"]) + 1;
            Response.Cookies.Append("UserCookieTentativas", result.ToString(), option);
            
        }

        public static void Executa()
        {
            //funciona
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                mcdh.Add(new Notificacao
                {
                    IdFormador = 1,
                    Turma = "Turma qq 1",
                    PathExcel = "path aqui 2",
                    Visualizacao = true
                });
                mcdh.SaveChanges();
            }
        }

        [Route("/")]
        public IActionResult IndexLogin()
        {
            //Executa();
            //var asd = new CookieOptions();
            //asd.Expires = DateTime.Now.AddMinutes(10);
            //Response.Cookies.Append("UserCookieTentativas", 0.ToString(), asd);
            //Someone sm = new Someone();
            Someone sm = CriaTentativasCookie(new Someone(0));

            if (sm.ContaTentativas >= 1 && HttpContext.Session.GetString("objJSONUsuario") == null)
            {
                ViewBag.display = "block;";
                ViewBag.MensagemErro = $"Login ou Senha Inválidos";
                ViewBag.TentativasRestantes = $"Tentativas restantes: { 5 - sm.ContaTentativas}";
                return View("Index", new ViewForModelLogin(sm));
            }
            if (HttpContext.Session.GetString("objJSONUsuario") != null)
                return RedirectToAction("IndexMainPage", "MainPage");
            ViewBag.display = "none;";

            return View("Index", new ViewForModelLogin(sm));
        }


        //Funciona com "login"
        //public void TentaLogar (Login login)
        //{
        //}


        //Não funciona só com o "l"
        //public void TentaLogar (Login l)
        //{
        //}

        public ActionResult TryLogin(Login login)
        {
            Someone sm = CriaTentativasCookie(new Someone(0));

            AutenticacaoDeLogin<Formador, Login> auth = new AutenticacaoDeLogin<Formador, Login>((f, l) => f.Username == l.Username && f.Password == l.Password);
            if (sm.ContaTentativas < 5)
                using (ModelCDHContext fc = new ModelCDHContext())
                {
                    Formador objFormador = fc?.Formador?.FirstOrDefault
                        (f => f.Username == (login.Username == null ? "Invalido" : login.Username)) ?? new Formador {Username = "404NOTFOUND!", Password="404NOTFOUND!" };

                    //SomeoneToView.Someone = sm;
                    if (auth.AutenticarUsuario(objFormador, login, sm)) {
                        sm.Formador = objFormador;
                        sm.Status = true;
                        HttpContext.Session.SetString("objJSONUsuario", JsonConvert.SerializeObject(sm));
                        return RedirectToAction("IndexMainPage", "MainPage");
                    }
                    else
                    {
                        AdicionaUmaTentativa();
                        return RedirectToAction("IndexLogin");
                    }

                    //return RedirectToAction("OtherMethod", "OtherController"); //justNames
                    //return RedirectToAction("Index", "MainPageController");
                }
            else
            {
                return RedirectToAction("IndexLogin");
            }
        }

        public ActionResult MainPage()
        {
            return View();
        }
    }
}
