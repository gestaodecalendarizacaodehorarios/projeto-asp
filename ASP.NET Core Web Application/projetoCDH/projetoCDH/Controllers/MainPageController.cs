﻿using Microsoft.AspNetCore.Mvc;
using projetoCDH.BLL.LoginAuth;
using projetoCDH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using projetoCDH.ViewModel;
using projetoCDH.Forms;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using OfficeOpenXml;
using static projetoCDH.Controllers.RetornadoresJSONFormadorController;

namespace projetoCDH.Controllers
{
    public class MainPageController : Controller
    {

        public Someone FromJsonSomeone
        {
            get => HttpContext.Session.GetString("objJSONUsuario") == null ? SomeoneNotFound.Someone : JsonConvert.DeserializeObject<Someone>(HttpContext.Session.GetString("objJSONUsuario"));
        }

        public bool Status { get => (bool)TempData["statusFormador"]; }

        #region Valida
        public ActionResult ValidaSomeonePaginaInicial()
        {
            if (FromJsonSomeone?.Status ?? false)
                return View("Index", new ViewForModel(FromJsonSomeone));
            else
                return RedirectToAction("IndexLogin", "Login");
        }
        public bool ValidaAdministrador()
        {

            if (FromJsonSomeone?.Formador?.IsAdmin ?? false && FromJsonSomeone?.Status == true)
                return true;
            else
                return false;
        }
        public bool ValidaUtilizador()
        {
            if (FromJsonSomeone?.Status == true)
                return true;
            else
                return false;
        }


        //[Route("/Download")]
        //public ActionResult DownloadExcel(string pathExcelPPaste)
        //{

        //}
        #endregion



        #region Info pagina inical

        public void PreecheInfoPagInicial()
        {
            #region info pag inicial
            int HorariosEnviados = 0;
            int HorariosParaEnviar = 0;
            int HorariosNaoVisto = 0;
            int HorariosTotal = 0;
            #endregion
            IEnumerable<Notificacao> notf;
            using (ModelCDHContext mcdh = new ModelCDHContext())
                notf = mcdh.Notificacao.Where(x => x.IdFormador == FromJsonSomeone.Formador.Id);

            foreach (var not in notf)
            {
                if (not.Inserido)
                    HorariosEnviados++;
                if (not.Inserido == false)
                    HorariosParaEnviar++;
                if (not.Visualizacao == false)
                    HorariosNaoVisto++;

                HorariosTotal++;
            }

        }

        #endregion
        [Route("/Index")]
        public ActionResult IndexMainPage()
        {
            return ValidaSomeonePaginaInicial();
        }

        #region Pages Criar
        [Route("/CriarTurma")]
        public ActionResult CriarTurma()
        {
            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    projetoCDH.Controllers.RetornadoresJsonController.TurmasPQuery = mcdh.Turma.ToList();
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        [Route("/CriarFormador")]
        public ActionResult CriarFormador()
        {


            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    projetoCDH.Controllers.RetornadoresJsonController.FormadoresPQuery = mcdh.Formador.ToList();
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        [Route("/CriarCurso")]
        public ActionResult CriarCurso()
        {
            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    projetoCDH.Controllers.RetornadoresJsonController.CursosPQuery = (mcdh.Curso.ToList());
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        [Route("/CriarModulo")]
        public ActionResult CriarModulo()
        {


            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    projetoCDH.Controllers.RetornadoresJsonController.ModulosPQuery = (mcdh.Modulo.ToList());
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");

        }
        [Route("/CriarCategoria")]
        public ActionResult CriarCategoria()
        {
            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    projetoCDH.Controllers.RetornadoresJsonController.GruposPQuery = (mcdh.Grupo.ToList());
                    projetoCDH.Controllers.RetornadoresJsonController.CategoriasPQuery = mcdh.Categoria.ToList();
                    projetoCDH.Controllers.RetornadoresJsonController.ModulosPQuery = (mcdh.Modulo.ToList().Where(x => x.CodModulo != 0));

                }
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        #endregion

        #region Pages Gerir

        [Route("/GerirFormadores")]
        public ActionResult GerirFormadores()
        {

            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    projetoCDH.Controllers.RetornadoresJsonController.FormadoresPQuery = (mcdh.Formador.ToList().Where(x => x.Id != 1));
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }
        [Route("/GerirCategorias")]
        public ActionResult GerirCategorias()
        {

            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    projetoCDH.Controllers.RetornadoresJsonController.CategoriasPQuery = (mcdh.Categoria.ToList().Where(x => x.Id != 1));
                    projetoCDH.Controllers.RetornadoresJsonController.ModulosPQuery = (mcdh.Modulo.ToList().Where(x => x.CodModulo != 0));
                }
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }
        [Route("/GerirModulos")]
        public ActionResult GerirModulos()
        {

            if (ValidaAdministrador())
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    projetoCDH.Controllers.RetornadoresJsonController.CategoriasPQuery = (mcdh.Categoria.ToList().Where(x => x.Id != 1));
                    projetoCDH.Controllers.RetornadoresJsonController.ModulosPQuery = (mcdh.Modulo.ToList().Where(x => x.CodModulo != 0));
                }
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }





        #endregion

        #region Pages Info
        [Route("/Horarios")]
        public ActionResult Horarios()
        {
            if (ValidaUtilizador())
            {
                int idFormadorLogado = FromJsonSomeone.Formador.Id;
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    projetoCDH.Controllers.RetornadoresJSONFormadorController.NotificacoesPQuery = (mcdh.Notificacao.ToList().Where(x => x.IdFormador == idFormadorLogado));
                    projetoCDH.Controllers.RetornadoresJSONFormadorController.TurmasPQuery = (mcdh.Turma.ToList());

                }
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        [Route("/TurmaAssociada")]
        public ActionResult TurmaAssociada()
        {
            if (ValidaUtilizador())
            {
                int idFormadorLogado = FromJsonSomeone.Formador.Id;
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    projetoCDH.Controllers.RetornadoresJSONFormadorController.NotificacoesPQuery = (mcdh.Notificacao.ToList().Where(x => x.IdFormador == idFormadorLogado));
                    projetoCDH.Controllers.RetornadoresJSONFormadorController.TurmasPQuery = (mcdh.Turma.ToList());

                }
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }
        #endregion

        #region Gerar horarios

        public ActionResult GerarHorarios()
        {
            if (ValidaAdministrador())
            {
                return View(new ViewForModel(FromJsonSomeone));
            }
            else
                return RedirectToAction("IndexLogin", "Login");
        }

        #endregion
        public ViewResult DashboardContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/Dashboard.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult _404ContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/404.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult BlankContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/blank.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult ButtonsContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/buttons.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult CardsContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/cards.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult ChartsContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/charts.cshtml", new ViewForModel(FromJsonSomeone));
        }

        public ViewResult ForgotPasswordContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/forgot-password.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult RegisterContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/register.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult TableContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/tables.cshtml"/*, new ViewForModel(FromJsonSomeone)*/);
        }
        public ViewResult UtilitiesAnimationContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/utilities-animation.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult UtilitiesBorderContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/utilities-border.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult UtilitiesColorContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/utilities-color.cshtml", new ViewForModel(FromJsonSomeone));
        }
        public ViewResult UtilitiesOtherContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/utilities-other.cshtml", new ViewForModel(FromJsonSomeone));
        }

        public ViewResult LoginContainerExample()
        {
            return View("~/Views/MainPage/OtherPages/login.cshtml", new ViewForModel(FromJsonSomeone));
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("IndexLogin", "Login");
        }

    }
}
