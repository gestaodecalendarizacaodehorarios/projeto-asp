﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using projetoCDH.Models;
using Newtonsoft.Json;
using Nager.Date;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace projetoCDH.Controllers
{
    public class ApiCalendarController : Controller
    {

        public class aData
        {
            public string data { get; set; }
            public bool direcao { get; set; }
        }


        public class bData
        {
            public DateTime data { get; set; }
            public bool disp { get; set; }
        }



        #region lerdatasfunc
        [HttpPost("/lerdatasfunc")]
        public JsonResult Read([FromBody]aData pedido)
        {
            Dictionary<string, List<string>> resposta = new Dictionary<string, List<string>>();
            List<string> datas = new List<string>();
            List<string> disp = new List<string>();
            PlanoFunc[] planoFuncs = new PlanoFunc[8];
            DateTime data = DateTime.Parse(pedido.data);
            DayOfWeek day = data.DayOfWeek;
            //string diastring = day.ToString();
            int dia = (int)day;
            if (dia == 0) dia = 7;
            if (dia>0 && dia <8)
            {
                while (dia != 1)
                {
                    dia -= 1;
                    data=data.AddDays(-1);
                }
                DateTime StartDate = data;
                DateTime EndDate = data.AddDays(7);

                List<PlanoFunc> dispDia;

                using (ModelCDHContext fc = new ModelCDHContext())
                {
                    dispDia = fc?.PlanoFunc?.ToList().Where(x => (DateTime)x.Data >= StartDate && (DateTime)x.Data <= EndDate).ToList();

                    //var disDia2 = from fccc in dispDia
                    //              where fccc.Data >= StartDate && fccc.Data <= EndDate;
                    data = StartDate;
                    for (int i = 0; i < 7; i++)
                    {
                        //var exists = dispDia.ElementAtOrDefault(i) != null;
                        if (dispDia.Count<7)
                        {
                            dispDia.Add(new PlanoFunc { Data = data.AddDays(i), Disp = true });     
                        }

                        datas.Add(dispDia[i].Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                        string tempBool;
                        if (dispDia[i].Disp==false)
                        {
                            tempBool = "False";
                        }
                        else
                        {
                            tempBool = "True";
                        }
                        disp.Add(tempBool);
                    }
                }
                for (int i=0;i<7; i++)
                {
                   
                    
                    //using (ModelCDHContext fc = new ModelCDHContext())
                    //{
                    //    //string dataString = data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //    DateTime dataHoje = DateTime.Now;
                    //    bool dbDisp=false;
                    //    //PlanoFunc dispDia = fc?.PlanoFunc?.FirstOrDefault
                    //    //    (f => f.Data == (data == null ? data : dataHoje)) ?? new PlanoFunc { Data = data.AddDays(i), Disp = false };
                    //    //planoFuncs[i] = dispDia;
                    //    //datas.Add(dispDia.Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    //    //disp.Add(dispDia.Disp.ToString());
                    //    //data=data.AddDays(1);
                    //}
                }
                resposta.Add("datas",datas);
                resposta.Add("disp", disp);
                return Json(resposta);
            }
            return Json("erro");
        }
        #endregion

        #region lerdatasfuncnav
        [HttpPost("/lerdatasfuncnav")]
        public JsonResult ReadNav([FromBody]aData pedido)
        {
            
            Dictionary<string, List<string>> resposta = new Dictionary<string, List<string>>();
            List<string> datas = new List<string>();
            List<string> disp = new List<string>();
            PlanoFunc[] planoFuncs = new PlanoFunc[8];
            DateTime data = DateTime.Parse(pedido.data);
            if (pedido.direcao == false)
            {
                data = data.AddDays(-1);
            }
            else
            {
                data = data.AddDays(1);
            }
            DayOfWeek day = data.DayOfWeek;
            //string diastring = day.ToString();
            
            int dia = (int)day;
            if (dia == 0) dia = 7;
            if (dia > 0 && dia < 8)
            {
                while (dia != 1)
                {
                    dia -= 1;
                    data = data.AddDays(-1);
                }
                DateTime StartDate = data;
                DateTime EndDate = data.AddDays(7);

                List<PlanoFunc> dispDia;

                using (ModelCDHContext fc = new ModelCDHContext())
                {
                    dispDia = fc?.PlanoFunc?.ToList().Where(x => (DateTime)x.Data >= StartDate && (DateTime)x.Data <= EndDate).ToList();

                    //var disDia2 = from fccc in dispDia
                    //              where fccc.Data >= StartDate && fccc.Data <= EndDate;
                    data = StartDate;
                    for (int i = 0; i < 7; i++)
                    {
                        //var exists = dispDia.ElementAtOrDefault(i) != null;
                        if (dispDia.Count < 7)
                        {
                            dispDia.Add(new PlanoFunc { Data = data.AddDays(i), Disp = true });
                        }

                        datas.Add(dispDia[i].Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                        string tempBool;
                        if (dispDia[i].Disp == false)
                        {
                            tempBool = "False";
                        }
                        else
                        {
                            tempBool = "True";
                        }
                        disp.Add(tempBool);
                    }
                }
                for (int i = 0; i < 7; i++)
                {


                    //using (ModelCDHContext fc = new ModelCDHContext())
                    //{
                    //    //string dataString = data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //    DateTime dataHoje = DateTime.Now;
                    //    bool dbDisp=false;
                    //    //PlanoFunc dispDia = fc?.PlanoFunc?.FirstOrDefault
                    //    //    (f => f.Data == (data == null ? data : dataHoje)) ?? new PlanoFunc { Data = data.AddDays(i), Disp = false };
                    //    //planoFuncs[i] = dispDia;
                    //    //datas.Add(dispDia.Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    //    //disp.Add(dispDia.Disp.ToString());
                    //    //data=data.AddDays(1);
                    //}
                }
                resposta.Add("datas", datas);
                resposta.Add("disp", disp);
                return Json(resposta);
            }
            return Json("erro");
        }
        #endregion

        #region gravarDisp
        [HttpPost("/gravarDisp")]
        public JsonResult Read([FromBody]string pedido)
        {
            //List<bData>  result = JsonConvert.DeserializeObject<List<bData>>(pedido);
            //foreach (bData item in result)
            //{
            //    DateTime data = item.data;
            //    bool disp = item.disp;
            //}
            var pedido2 = System.Text.RegularExpressions.Regex.Unescape(pedido);
            var dict = JObject.Parse(pedido2);
            var dict2 = JsonConvert.DeserializeObject<dynamic>(pedido2);
            JToken jUser = dict2["dict"];
            var name = (string)jUser["22/07/2018"];
            List<string> datas = new List<string>();
            foreach (string item in jUser)
            {
                datas.Add(item);
            };
            DateTime dataSegunda = DateTime.ParseExact(datas[7], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<PlanoFunc> ListPlanoFunc = new List<PlanoFunc>();
            for (int i = 0; i < 7; i++)
            {
                PlanoFunc planoFunc = new PlanoFunc();
                planoFunc.Data = dataSegunda;
                if (datas[i] == "False")
                {
                    planoFunc.Disp = false;
                }
                else
                {
                    planoFunc.Disp = true;
                }
                List<PlanoFunc> dispDia;
                using (ModelCDHContext mcdh = new ModelCDHContext())
                {
                    dispDia = mcdh?.PlanoFunc?.ToList().Where(x => (DateTime)x.Data >= planoFunc.Data && (DateTime)x.Data <= planoFunc.Data).ToList();
                    if (dispDia.Count<1)
                    {
                        mcdh.PlanoFunc.Add(planoFunc);
                        mcdh.SaveChanges();
                    }
                    else
                    {
                        LinqSaveDisp(planoFunc);
                    }
                    
                }
                dataSegunda = dataSegunda.AddDays(1);
            }
            //var oMycustomclassname = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(pedido);
            //PlanoFunc datas = (PlanoFunc)JsonConvert.DeserializeObject(pedido);
            //List<PlanoFunc> planoFuncList = new List<PlanoFunc>();
            //foreach (JToken item in obj["dict"]) //KeyValuePair<string, JToken>
            //{
            //    PlanoFunc planoFunc = new PlanoFunc();
            //    planoFunc.Data = DateTime.ParseExact(item.Key, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //    planoFunc.Disp = (bool)item.Value;
            //    planoFuncList.Add(planoFunc);
            //}
            //using (ModelCDHContext mcdh = new ModelCDHContext())
            //{
            //    foreach (var item in pedido)
            //    {
            //        mcdh.PlanoFunc.Add(item);
            //        mcdh.SaveChanges();
            //    }

            //}
            return new JsonResult(true);
        }
        #endregion

        #region LinqSaveDisp
        public static bool LinqSaveDisp(PlanoFunc planoFunc)
        {
            ModelCDHContext mcdh = new ModelCDHContext();
            PlanoFunc dispDia = mcdh.PlanoFunc.Single(x => (DateTime)x.Data == planoFunc.Data);
            dispDia.Data = planoFunc.Data;
            dispDia.Disp = planoFunc.Disp;
            mcdh.SaveChanges();
            return true;
        }
        #endregion

        #region navegarsemana
        //[HttpPost("/navegarsemana")]
        //public JsonResult ReadAfrente(aData pedido)
        //{
        //    Dictionary<string, List<string>> resposta = new Dictionary<string, List<string>>();
        //    List<string> datas = new List<string>();
        //    List<string> disp = new List<string>();
        //    PlanoFunc[] planoFuncs = new PlanoFunc[8];
        //    DateTime data = DateTime.Parse(pedido.data);
        //    if (pedido.direcao == true)
        //    {
        //        data = data.AddDays(1);
        //    }
        //    if (pedido.direcao == false)
        //    {
        //        data = data.AddDays(-1);
        //    }
        //    DayOfWeek day = data.DayOfWeek;
        //    //string diastring = day.ToString();
        //    int dia = (int)day;
        //    if (dia == 0) dia = 7;
        //    if (dia > 0 && dia < 8)
        //    {
        //        while (dia != 1)
        //        {
        //            dia -= 1;
        //            data = data.AddDays(-1);
        //        }
        //        DateTime StartDate = data;
        //        DateTime EndDate = data.AddDays(7);

        //        List<PlanoFunc> dispDia;

        //        using (ModelCDHContext fc = new ModelCDHContext())
        //        {
        //            dispDia = fc?.PlanoFunc?.ToList().Where(x => (DateTime)x.Data >= StartDate && (DateTime)x.Data <= EndDate).ToList();

        //            //var disDia2 = from fccc in dispDia
        //            //              where fccc.Data >= StartDate && fccc.Data <= EndDate;
        //            data = StartDate;
        //            for (int i = 0; i < 7; i++)
        //            {
        //                //var exists = dispDia.ElementAtOrDefault(i) != null;
        //                if (dispDia.Count < 7)
        //                {
        //                    dispDia.Add(new PlanoFunc { Data = data.AddDays(i), Disp = true });
        //                }

        //                datas.Add(dispDia[i].Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
        //                string tempBool;
        //                if (dispDia[i].Disp == false)
        //                {
        //                    tempBool = "False";
        //                }
        //                else
        //                {
        //                    tempBool = "True";
        //                }
        //                disp.Add(tempBool);
        //            }
        //        }
        //        for (int i = 0; i < 7; i++)
        //        {


        //            //using (ModelCDHContext fc = new ModelCDHContext())
        //            //{
        //            //    //string dataString = data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        //            //    DateTime dataHoje = DateTime.Now;
        //            //    bool dbDisp=false;
        //            //    //PlanoFunc dispDia = fc?.PlanoFunc?.FirstOrDefault
        //            //    //    (f => f.Data == (data == null ? data : dataHoje)) ?? new PlanoFunc { Data = data.AddDays(i), Disp = false };
        //            //    //planoFuncs[i] = dispDia;
        //            //    //datas.Add(dispDia.Data.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
        //            //    //disp.Add(dispDia.Disp.ToString());
        //            //    //data=data.AddDays(1);
        //            //}
        //        }
        //        resposta.Add("datas", datas);
        //        resposta.Add("disp", disp);
        //        return Json(resposta);
        //    }
        //    return Json("erro");
        //}
        #endregion
    }
}
