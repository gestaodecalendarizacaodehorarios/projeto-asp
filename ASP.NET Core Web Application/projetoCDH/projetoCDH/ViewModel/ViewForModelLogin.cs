﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using projetoCDH.BLL.LoginAuth;
using projetoCDH.Forms;

namespace projetoCDH.ViewModel
{
    public class ViewForModelLogin
    {
        public ViewForModelLogin(Someone sm)
        {
            this.Someone = sm;
        }

        public Login Login { get; set; }

        public Someone Someone { get; set; }
        public bool TimeWaiting { get; set; } = false;
    }
}
