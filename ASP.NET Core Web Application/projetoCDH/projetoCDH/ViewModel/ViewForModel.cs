﻿using projetoCDH.BLL.LoginAuth;
using projetoCDH.Forms;
using projetoCDH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using projetoCDH.BLL.SelectInBD;
using static projetoCDH.BLL.SelectInBD.QueriesPersonalizadas;

namespace projetoCDH.ViewModel
{
    public class ViewForModel
    {
        public ViewForModel(Someone sm)
        {
            Someone = sm;
            Formador = sm.Formador;
        }
        public Someone Someone { get; set; }
        public Formador Formador { get; set; }

        public IEnumerable<Formador> ListaDeFormadores
        {
            get
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                  return mcdh.Formador.ToList().Where( x => x.Id != 1);
            }
        }

        public IEnumerable<Regime> ListaDeRegimes
        {
            get
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    return mcdh.Regime.ToList();
            }

        }

        public IEnumerable<Curso> ListaDeCursos
        {
            get
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    return mcdh.Curso.ToList();
            }
        }

        public List<FormadorECurso> DocentesESigla
        {
            get { return QueriesPersonalizadas.RetornaNomeESiglaDoCursoDoFormador(); }
        }

        public IEnumerable<Modulo> ListaDeModulos
        {
            get { return projetoCDH.Controllers.RetornadoresJsonController.ModulosPQuery; }
        }

        public IEnumerable<Categoria> ListaDeCategorias
        {
            get { return projetoCDH.Controllers.RetornadoresJsonController.CategoriasPQuery; }
        }

        public IEnumerable<Grupo> ListaDeGrupos
        {
            get { return projetoCDH.Controllers.RetornadoresJsonController.GruposPQuery; }
        }

        public IEnumerable<Notificacao> ListaDeNotificacao
        {
            get
            {
                using (ModelCDHContext mcdh = new ModelCDHContext())
                    return mcdh.Notificacao.ToList();
            }
        }


        public CriacaoDaTurma criacaoDaTurma { get; set; }
        public CriacaoDoFormador criacaoDoFormador { get; set; }
        public UploadFileExcel UploadFileExcel { get; set; }

        #region info pag inicial
        public int HorariosEnviados { get; set; }
        public int HorariosParaEnviar { get; set; }
        public int HorariosNaoVisto { get; set; }
        public int HorariosTotal { get; set; }
        #endregion
    }
}
