﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Nager.Date;

namespace testesExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            CalendarioProvisorio.Executa();
            CalendarioProvisorio.CalculaDataFinalTPSI(DateTime.Parse("25/10/2019"));
            
            Console.WriteLine("Hello World!");
        }
    }


    public class CalendarioProvisorio
    {
        //return the date between two dates
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
        // print it or whatever
        //foreach (DateTime day in EachDay(StartDate, EndDate))
        public static void Executa()
        {
            //TPSI TEM 1775 horas, 6 por dia em dias úteis

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(@"E:\IN LIVE\GIT\GITLAB\projeto-asp\ASP.NET Core Web Application\projetoCDH\testesExcel\Disponibilidade-formador.xlsx");
            Excel.Worksheet xlWorkSheet = xlWorkBook.Sheets[1];
            Excel.Range xlRange = xlWorkSheet.UsedRange;

            //xlWorkSheet.Rows["1"].Style.Orientation = -90;


            int totalRows = xlWorkSheet.UsedRange.Rows.Count;
            int totalColumns = xlWorkSheet.UsedRange.Columns.Count;

            string firstValue, secondValue;

            for (int rowCount = 1; rowCount <= totalRows; rowCount++)
            {

                firstValue = Convert.ToString((xlRange.Cells[rowCount, 1] as Excel.Range).Text);
                secondValue = Convert.ToString((xlRange.Cells[rowCount, 2] as Excel.Range).Text);

                Console.WriteLine(firstValue + "\t" + secondValue);

            }

            xlWorkBook.Close();
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            Console.WriteLine("End of the file...");
        }

        public static DateTime CalculaDataFinalTPSI(DateTime DataInicio)
        {
            //(1775/6)
            //296 dias de tpsi;
            var daysToAdd = 296;
            DateTime dataFinal = DataInicio;

            var weeks = DateSystem.GetWeekendProvider(CountryCode.PT);

            while (daysToAdd > 0)
            {
                dataFinal = dataFinal.AddDays(1);

                if (!weeks.IsWeekend(dataFinal) &&
                    !DateSystem.IsPublicHoliday(dataFinal, CountryCode.PT) && dataFinal.Month != 8)
                {
                    daysToAdd -= 1;
                }
            }

            return dataFinal;
            //Data FINAL 29/01/2021 c ferias
        }

    }




}
