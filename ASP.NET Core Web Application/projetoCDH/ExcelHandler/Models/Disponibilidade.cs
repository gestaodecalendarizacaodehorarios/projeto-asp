﻿using System;
using System.Collections.Generic;

namespace ExcelHandler.Models
{
    public partial class Disponibilidade
    {
        public int IdFormador { get; set; }
        public DateTime Data { get; set; }
        public bool H0830 { get; set; }
        public bool H0930 { get; set; }
        public bool H1040 { get; set; }
        public bool H1140 { get; set; }
        public bool H1330 { get; set; }
        public bool H1430 { get; set; }
        public bool H1540 { get; set; }
        public bool H1650 { get; set; }
        public bool H1750 { get; set; }
        public bool H1915 { get; set; }
        public bool H2015 { get; set; }
        public bool H2125 { get; set; }
        public bool H2225 { get; set; }

        public Formador IdFormadorNavigation { get; set; }
    }
}
