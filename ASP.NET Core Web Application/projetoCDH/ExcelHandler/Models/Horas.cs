﻿using System;
using System.Collections.Generic;

namespace ExcelHandler.Models
{
    public partial class Horas
    {
        public Horas()
        {
            HorarioTurmas = new HashSet<HorarioTurmas>();
            HorasRegime = new HashSet<HorasRegime>();
        }

        public int Id { get; set; }
        public string Hora { get; set; }

        public ICollection<HorarioTurmas> HorarioTurmas { get; set; }
        public ICollection<HorasRegime> HorasRegime { get; set; }
    }
}
