﻿using System;
using System.Collections.Generic;

namespace ExcelHandler.Models
{
    public partial class Regime
    {
        public Regime()
        {
            HorasRegime = new HashSet<HorasRegime>();
            Turma = new HashSet<Turma>();
        }

        public int Id { get; set; }
        public string Regime1 { get; set; }

        public ICollection<HorasRegime> HorasRegime { get; set; }
        public ICollection<Turma> Turma { get; set; }
    }
}
