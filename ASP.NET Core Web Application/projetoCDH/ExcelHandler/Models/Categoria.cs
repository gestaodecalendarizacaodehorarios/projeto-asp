﻿using System;
using System.Collections.Generic;

namespace ExcelHandler.Models
{
    public partial class Categoria
    {
        public Categoria()
        {
            OrdemModCat = new HashSet<OrdemModCat>();
        }

        public int Id { get; set; }
        public int? IdGrupo { get; set; }
        public string Nome { get; set; }
        public int Ordem { get; set; }

        public Grupo IdGrupoNavigation { get; set; }
        public ICollection<OrdemModCat> OrdemModCat { get; set; }
    }
}
