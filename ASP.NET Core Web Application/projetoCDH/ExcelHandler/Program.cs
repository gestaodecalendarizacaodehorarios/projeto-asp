﻿using System;
using System.Diagnostics;
using System.Linq;
using ExcelHandler.ExcelCalendarioProvisorio;
using ExcelHandler.Models;

namespace ExcelHandler
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            InfoParaUmHorario infUmHorario = new InfoParaUmHorario();

            string pathDiurno = infUmHorario.HorarioDiurno(new InfoParaUmHorario
            {
                //DataDeInicio = DateTime.ParseExact("1/11/2019", "MM/dd/yyyy", CultureInfo.InvariantCulture),
                DataDeInicio = DateTime.UtcNow,
                NmrTotalDeHorasCurso = 2000,
                MesDeFerias = 3,
                NomeDaTurma = "Uma turma Diurna",
                RegimeTurma = "Diurno"

            });

            using (ModelCDHContext mdch = new ModelCDHContext())
            {
                Formador[] fm = mdch.Formador.ToArray();
                infUmHorario.CriaHorarioParaOsFormadoresDiurnos(fm, pathDiurno);

            }


            Console.WriteLine("--Gerando o horário--");
            Console.WriteLine("Tempo de duração:");
            TimeSpan ts = sw.Elapsed;
            sw.Stop();
            Console.WriteLine(ts.TotalSeconds);
            Console.WriteLine();
            Console.WriteLine($"\n\nHorários gerados");
            Console.WriteLine();

            Console.WriteLine("Hello World!");
        }
    }
}
