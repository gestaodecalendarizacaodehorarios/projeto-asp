﻿
--use master
--go

--create database projecto_final;
--go 

--use projecto_final;

create table formador(
    id int IDENTITY(1,1) primary key,
    nome nvarchar(255) not null,
    username nvarchar(255) not null,
    passwd nvarchar(255) not null,
    email nvarchar(255) not null,
    is_admin bit default 0,
);

create table disponibilidade(
	id_formador int foreign key references formador(id), 
	data date not null,
    h0830 bit not null,
    h0930 bit not null,
    h1040 bit not null,
    h1140 bit not null,
    h1330 bit not null,
    h1430 bit not null,
    h1540 bit not null,
    h1650 bit not null,
    h1750 bit not null,
    h1915 bit not null,
    h2015 bit not null,
    h2125 bit not null,
    h2225 bit not null    
);

create table horas(
    id int IDENTITY(1,1) primary key,
    hora nvarchar(255)
);

create table modulo(
    id int IDENTITY(1,1) primary key,
    nome nvarchar(255) not null,
    nmr_horas int not null,
    observacoes nvarchar(255)
);

create table regime(
    id int IDENTITY(1,1) primary key,
    regime nvarchar(255)
);


create table curso(
    id int IDENTITY(1,1) primary key,
    nome nvarchar(255),
    sigla nvarchar(25) not null
);

create table turma(
	id int IDENTITY(1,1) primary key,
	id_regime int references regime(id),
	id_curso int references curso(id),
	nome nvarchar(255) not null,
    sigla nvarchar(25) not null,
	data_inicio date not null,
	coordenador nvarchar(255) not null,
);

create table formador_modulo(
	id_formador int references formador(id),
	id_modulo int references modulo(id),
    id_turma int references turma(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT PK_formador_modulo PRIMARY KEY (id_formador,id_modulo,id_turma)
);


create table horario_turmas(
    data date not null,
	id_modulo int references modulo(id),
    id_hora int references horas(id),
	id_turma int references turma(id) ON DELETE CASCADE ON UPDATE CASCADE
);



create table horas_regime(
    id_hora int references horas(id),
    id_regime int references regime(id),
	CONSTRAINT PK_horas_regime PRIMARY KEY (id_hora,id_regime)

);


create table grupo(
    id int IDENTITY(1,1) primary key,
    id_curso int references curso(id) ON DELETE CASCADE  ON UPDATE CASCADE,
    nome nvarchar(255),
    ordem int not null
);


create table categoria(
    id int IDENTITY(1,1) primary key,
    id_grupo int references grupo(id) ON DELETE CASCADE  ON UPDATE CASCADE,
    nome nvarchar(255),
    ordem int not null
);

create table ordem_mod_cat(
    id_modulo int references modulo(id),
    id_categoria int references categoria(id) ON DELETE CASCADE ON UPDATE CASCADE,
    ordem int not null,
	CONSTRAINT PK_ordem_mod_cat PRIMARY KEY (id_modulo,id_categoria)
);






