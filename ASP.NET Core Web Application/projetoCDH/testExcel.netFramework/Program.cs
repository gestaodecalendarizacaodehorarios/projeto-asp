﻿using Nager.Date;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using testExcel.netFramework.Encriptacao;

namespace testExcel.netFramework
{
    class Program
    {

        static void Main(string[] args)
        {
            #region Cria horários
            //Task t1 = new Task(CalendarioProvisorio.Executa);
            ////Task t1 = new Task(CalendarioProvisorio.LeCorExcelTOP);
            //Stopwatch sw = new Stopwatch();
            ////sw.Start();

            //Console.WriteLine("--Criando os horários--");
            //Console.WriteLine("Tempo de duração:");
            //TimeSpan ts;

            //while (!t1.IsCompleted)
            //{
            //    ts = sw.Elapsed;
            //    Console.Write(ts.Seconds + "s ");
            //    System.Threading.Thread.Sleep(1000);
            //}
            //sw.Stop();
            //t1.Wait();
            //Console.WriteLine("\n\nHorários gerados!!");
            #endregion
            #region Testando pula semana
            //DateTime today = DateTime.Today;
            //// The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            //var som = ((int)DayOfWeek.Monday - (int)today.DayOfWeek + 7);
            //int daysUntilTuesday = ((int)DayOfWeek.Monday - (int)today.DayOfWeek + 7) % 7;
            //DateTime nextMonday = today.AddDays(daysUntilTuesday);
            #endregion


            #region testando dados encriptados

            Criptografa cp = new Criptografa();

            Console.WriteLine("Please enter a password to use:");
            string password = Console.ReadLine();
            Console.WriteLine("Please enter a string to encrypt:");
            string plaintext = Console.ReadLine();
            Console.WriteLine("");

            Console.WriteLine("Your encrypted string is:");
            string encryptedstring = cp.Encrypt(plaintext, password);
            Console.WriteLine(encryptedstring);
            Console.WriteLine("");

            Console.WriteLine("Your decrypted string is:");
            string decryptedstring = Decriptografa.Decrypt(encryptedstring, password);
            Console.WriteLine(decryptedstring);
            Console.WriteLine("");

            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();

            Console.WriteLine();
            Console.ReadLine();
            #endregion
        }


    }
    public class CalendarioProvisorio
    {
        private static void KillSpecificExcelFileProcess(string excelFileName)
        {
            var processes = from p in Process.GetProcessesByName("EXCEL")
                            select p;

            foreach (var process in processes)
            {
                if (process.MainWindowTitle == "Microsoft Excel - " + excelFileName)
                    process.Kill();
            }
        }
        //return the date between two dates
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        //print it or whatever
        //foreach (DateTime day in EachDay(StartDate, EndDate))
        public static void Executa()
        {
            //CriaArquivoEExcelESalvaCValidacoesObrigatorias(DateTime.Parse("26/10/2019"));
            Task tsk1 = Task.Run(() =>
            {
                CriaHorarioDiurno(DateTime.Parse("26/10/2019"));

            });
            Task tsk2 = Task.Run(() =>
            {
                CriaHorarioDaTarde(DateTime.Parse("26/10/2019"));
            });

            Task tsk3 = Task.Run(() =>
            {
                CriaHorarioPosLaboral(DateTime.Parse("26/10/2019"));
            });

            //LeCorExcelTOP();
        }

        public static void LeCorExcelTOP()
        {
            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook = xlsApp.Workbooks.Open(@"E:\IN LIVE\GIT\GITLAB\projeto-asp\ASP.NET Core Web Application\projetoCDH\testesExcel\Disponibilidade-formador.xlsx");
            Excel.Worksheet xlsWorkSheet = xlsWorkBook.Sheets[1];
            var gray = xlsWorkSheet.Cells[10, 3].Interior.Color;
            //11184814 - cinza
            //255 - vermelho
            Console.WriteLine("Pegado");

        }

        public static void CriaArquivoEExcelESalvaCValidacoesObrigatorias(DateTime DataInicio)
        {
            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinalTPSI(DataInicio);



            //mata processo
            KillSpecificExcelFileProcess("Criadela.xlsx");
            if (File.Exists(Directory.GetCurrentDirectory() + "/Criadela.xlsx"))
                File.Delete(Directory.GetCurrentDirectory() + "/Criadela.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet;

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = "Sheet um";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/Criadela.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ Criadela.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = "ID\ndo\nFormador";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1] = "Nome\ndo\nFormador";

                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";


                #endregion


                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion


                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

                column = 3;
                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    if (DateSystem.IsWeekend(day, CountryCode.PT) || DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == 8)
                    {
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";
                    }

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs(Directory.GetCurrentDirectory() + "/Criadela.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                throw;
            }
        }

        public static void CriaHorarioDiurno(DateTime DataInicio, object formador = null)
        {
            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinalTPSI(DataInicio);

            //mata processo
            KillSpecificExcelFileProcess("HorarioDiurno.xlsx");
            if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx"))
                File.Delete(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet;

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = "Sheet um";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDiurno.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = "ID\ndo\nFormador";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9, 2] = "Disponível";
                xlsWorkSheet.Cells[9, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9, 3].Interior.Color = "5296274";



                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1] = "Nome\ndo\nFormador";

                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                 xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "5296274";

                column = 3;
                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    //xlsWorkSheet.Range[xlsWorkSheet.Cells[5, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";

                    if (DateSystem.IsWeekend(day, CountryCode.PT) || DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == 8)
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column-1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs(Directory.GetCurrentDirectory() + "/HorarioDiurno.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                throw;
            }
            Console.WriteLine("\n\nHorario diurno criado!");
        }

        public static void CriaHorarioDaTarde(DateTime DataInicio, object formador = null)
        {
            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinalTPSI(DataInicio);


            //mata processo
            KillSpecificExcelFileProcess("HorarioDaTarde.xlsx");
            if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx"))
                File.Delete(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet;

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = "Sheet um";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioDaTarde.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = "ID\ndo\nFormador";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9,2] = "Disponível";
                xlsWorkSheet.Cells[9,3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9,3].Interior.Color = "5296274";
                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1] = "Nome\ndo\nFormador";

                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[5, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

                column = 3;
                //retira feriados do horario da tarde
                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {

                    if (DateSystem.IsWeekend(day, CountryCode.PT) || DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == 8)
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[4, column - 1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs(Directory.GetCurrentDirectory() + "/HorarioDaTarde.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                throw;
            }
            Console.WriteLine("\n\nHorario da tarde criado!");
        }

        public static void CriaHorarioPosLaboral(DateTime DataInicio, object formador = null)
        {
            //calculaDataFinal
            DateTime DataFinalCurso = CalculaDataFinalTPSI(DataInicio);


            //mata processo
            KillSpecificExcelFileProcess("HorarioPosLaboral.xlsx");
            if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx"))
                File.Delete(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx");

            Excel.Application xlsApp = new Excel.Application();
            Excel.Workbook xlsWorkBook;
            Excel.Worksheet xlsWorkSheet;

            xlsWorkBook = (Excel.Workbook)(xlsApp.Workbooks.Add(Missing.Value));
            xlsWorkSheet = (Excel.Worksheet)xlsWorkBook.ActiveSheet;

            xlsWorkSheet.Name = "Sheet um";

            //if (File.Exists(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx"))
            //    xlsWorkBook = xlsApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/ HorarioPosLaboral.xlsx");
            //Adiciona sheet

            try
            {

                #region Row 1
                xlsWorkSheet.Range["A1:QT1"].NumberFormat = "dd/mm/yyyy";
                xlsWorkSheet.Range["1:1"].Cells.Orientation = Excel.XlOrientation.xlUpward;

                var FirstCell = (Excel.Range)xlsWorkSheet.Cells[1, 1];
                FirstCell.RowHeight = 69;


                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 1], xlsWorkSheet.Cells[6, 1]].Merge();

                xlsWorkSheet.Columns[1].ColumnWidth = 15;
                xlsWorkSheet.Columns[2].ColumnWidth = 15;

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].NumberFormat = "General";

                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[1, 1], xlsWorkSheet.Cells[1, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[1, 1] = "ID\ndo\nFormador";

                xlsWorkSheet.Cells[1, 2] = "Horas";

                xlsWorkSheet.Cells[1, 1].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                xlsWorkSheet.Cells[1, 2].Cells.Orientation = Excel.XlOrientation.xlHorizontal;

                #endregion
                #region Row 2

                xlsWorkSheet.Rows[2].Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlsWorkSheet.Cells[8, 2] = "Indisponível";
                xlsWorkSheet.Cells[8, 3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[8, 3].Interior.Color = "255";
                xlsWorkSheet.Cells[9,2] = "Disponível";
                xlsWorkSheet.Cells[9,3].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                xlsWorkSheet.Cells[9,3].Interior.Color = "5296274";
                #endregion
                #region Row 3

                xlsWorkSheet.Cells[3, 1].HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1].VerticalAlignment =
                Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Cells[3, 1] = "Nome\ndo\nFormador";

                #endregion

                #region Conf horas

                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].HorizontalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 2], xlsWorkSheet.Cells[6, 2]].VerticalAlignment =
                     Excel.XlVAlign.xlVAlignCenter;

                xlsWorkSheet.Cells[3, 2] = "08:30/12:40";
                xlsWorkSheet.Cells[4, 2] = "13:30/16:40";
                xlsWorkSheet.Cells[5, 2] = "16:50/18:50";
                xlsWorkSheet.Cells[6, 2] = "19:15/23:25";

                #endregion

                xlsWorkSheet.Columns.AutoFit();


                int column = 3;

                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {
                    #region Row 1

                    xlsWorkSheet.Cells[1, column] = day;
                    xlsWorkSheet.Cells[1, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 2
                    DayOfWeek dofw = day.DayOfWeek;
                    xlsWorkSheet.Cells[2, column] = dofw.ToString()[0].ToString();
                    xlsWorkSheet.Cells[2, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 3
                    xlsWorkSheet.Cells[3, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 4
                    xlsWorkSheet.Cells[4, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    #region Row 5
                    xlsWorkSheet.Cells[5, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion 

                    #region Row 6
                    xlsWorkSheet.Cells[6, column].Borders.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    #endregion

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }

                xlsWorkSheet.Range[xlsWorkSheet.Cells[6, 3], xlsWorkSheet.Cells[6, column - 1]].Interior.Color = "5296274";

                column = 3;
                for (var day = DataInicio.Date; day.Date <= DataFinalCurso.Date; day = day.AddDays(1))
                {

                    if (DateSystem.IsWeekend(day, CountryCode.PT) || DateSystem.IsPublicHoliday(day, CountryCode.PT) || day.Month == 8)
                        xlsWorkSheet.Range[xlsWorkSheet.Cells[2, column], xlsWorkSheet.Cells[6, column]].Interior.Color = "11184814";

                    ////Usar cellls
                    //xlsWorkSheet.Cells[2, column].Cells.Orientation = Excel.XlOrientation.xlHorizontal;
                    column++;
                }
                xlsWorkSheet.Range[xlsWorkSheet.Cells[3, 3], xlsWorkSheet.Cells[5, column-1]].Interior.Color = "11184814";

                xlsWorkSheet.Columns.AutoFit();

                xlsWorkBook.SaveAs(Directory.GetCurrentDirectory() + "/HorarioPosLaboral.xlsx");

                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
            }
            catch (Exception)
            {
                xlsWorkBook.Close(true);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkBook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsWorkSheet);
                xlsApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(xlsApp);
                throw;
            }
            Console.WriteLine("\n\nHorario pós-laboral criado!");
        }

        public static DateTime CalculaDataFinalTPSI(DateTime DataInicio)
        {
            //(1775 / 6)
            //    296 dias de tpsi;
            var daysToAdd = 296;
            DateTime dataFinal = DataInicio;

            var weeks = DateSystem.GetWeekendProvider(CountryCode.PT);

            while (daysToAdd > 0)
            {
                dataFinal = dataFinal.AddDays(1);

                if (!weeks.IsWeekend(dataFinal) &&
                    !DateSystem.IsPublicHoliday(dataFinal, CountryCode.PT) && dataFinal.Month != 8)
                {
                    daysToAdd -= 1;
                }
            }

            return dataFinal;
            //Data FINAL 29 / 01 / 2021 c ferias
        }

    }
}
