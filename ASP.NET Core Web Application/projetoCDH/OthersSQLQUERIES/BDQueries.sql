﻿--ALTER TABLE Formador
--DROP Column id_utilizador

--ALTER TABLE Formador
--DROP CONSTRAINT fk_Formador_Utilizador;

--ALTER TABLE Formador
--ADD id_utilizador int
----
--ALTER TABLE Formador
--ADD username varchar(30), [password] varchar(30), is_admin BIT
----
--drop table Utilizador

----
--ALTER TABLE Formador
--ADD CONSTRAINT fk_Formador_Utilizador FOREIGN KEY(id_utilizador) References Utilizador(id);

--drop table Formador


--drop table Utilizador

CREATE TABLE Utilizador(
[id] INT IDENTITY(1,1) Primary key,
[username] VARCHAR(50) CONSTRAINT username_unico UNIQUE,
[password] VARCHAR(20),
[is_admin] BIT
)

CREATE TABLE Formador(
[id] INT IDENTITY(1,1),
[nome] VARCHAR(100),
[email] VARCHAR(30) CONSTRAINT email_unico UNIQUE,
[categoria] VARCHAR(50),
[id_utilizador] INT,
CONSTRAINT pk_Formador PRIMARY KEY CLUSTERED ([id]),
CONSTRAINT FK_Formador FOREIGN KEY ([id_utilizador]) REFERENCES Utilizador([id])


)
CREATE TABLE Regime(
[id] INT IDENTITY(1,1),
[regime] VARCHAR(50),
CONSTRAINT pk_Regime PRIMARY KEY CLUSTERED ([id],[regime])
)

CREATE TABLE Curso(
[id] INT IDENTITY(1,1),
[nome] VARCHAR(50) UNIQUE,
[id_regime] INT,
[nome_regime] VARCHAR(50),
CONSTRAINT PK_Curso PRIMARY KEY CLUSTERED ([id]),
CONSTRAINT FK_Curso FOREIGN KEY ([id_regime],[nome_regime]) REFERENCES Regime([id],[regime])
)

CREATE TABLE Turma(
[id] INT IDENTITY(1,1),
[id_curso] INT,
[nome] VARCHAR(30),
[data_inicio] DATETIME,
[coordenador] VARCHAR(30),
CONSTRAINT pk_Turma PRIMARY KEY CLUSTERED ([id]),
CONSTRAINT FK_Turma FOREIGN KEY([id_curso]) REFERENCES Curso([id])
)

CREATE TABLE Formador_Turma(
[id_turma] INT,
[id_formador] INT,
CONSTRAINT pk_Formador_Turma PRIMARY KEY CLUSTERED ([id_turma], [id_formador]),
CONSTRAINT FK_Formador_Turma FOREIGN KEY ([id_turma]) REFERENCES Turma([id]),
CONSTRAINT FK_Formador_Turma2 FOREIGN KEY([id_formador]) REFERENCES Formador([id])
)



CREATE TABLE Horario_Turmas(
[id_turma] INT,
[data] DATE,
[h0830] DATE,
[h0930] DATE,
[h1040] DATE,
[h1140] DATE,
[h1330] DATE,
[h1430] DATE,
[h1540] DATE,
[h1650] DATE,
[h1750] DATE,
[h1915] DATE,
[h2015] DATE,
[h2115] DATE,
[h2225] DATE,
CONSTRAINT pk_Horario_Turmas20 PRIMARY KEY CLUSTERED ([id_turma], [data]),
CONSTRAINT FK_Horario_Turmas FOREIGN KEY ([id_turma]) REFERENCES Turma([id])
)

CREATE TABLE Categoria(
[id] INT IDENTITY(1,1),
[nome] VARCHAR(30),
CONSTRAINT pk_Categoria PRIMARY KEY CLUSTERED([id])
)

CREATE TABLE Modulo(
[id] INT IDENTITY(1,1),
[cod_modulo] VARCHAR(36),
[nome] VARCHAR(30),
[id_categoria] INT,
CONSTRAINT FK_Modulo FOREIGN KEY ([id_categoria]) REFERENCES Categoria([id]),
CONSTRAINT pk_Modulo PRIMARY KEY CLUSTERED ([id],[cod_modulo])
)

CREATE TABLE Modulos_Curso(
[id] INT IDENTITY(1,1),
[id_curso] INT,
[id_modulo] INT,
[cod_modulo] VARCHAR(36),
[ordem] VARCHAR(30),
CONSTRAINT pk_Horario_Turmas PRIMARY KEY CLUSTERED ([id]),
CONSTRAINT FK_Modulos_Curso FOREIGN KEY ([id_curso]) REFERENCES Curso([id]),
CONSTRAINT FK_Modulos_Modulo FOREIGN KEY ([id_modulo],[cod_modulo]) REFERENCES Modulo([id],[cod_modulo])
)

CREATE TABLE Formador_Categoria(
[id_categoria] INT,
[id_formador] INT,
CONSTRAINT pk_Formador_Categoria PRIMARY KEY CLUSTERED([id_categoria],[id_formador]),
CONSTRAINT FK_Formador_Categoria FOREIGN KEY ([id_formador]) REFERENCES Formador([id]),
CONSTRAINT FK_Formador_Categoria2 FOREIGN KEY ([id_categoria]) REFERENCES Categoria([id])
)

CREATE TABLE Disponibilidade(
[id] INT IDENTITY(1,1),
[id_formador] INT,
[data] DATE,
[h0830] DATE,
[h0930] DATE,
[h1040] DATE,
[h1140] DATE,
[h1330] DATE,
[h1430] DATE,
[h1540] DATE,
[h1650] DATE,
[h1750] DATE,
[h1915] DATE,
[h2015] DATE,
[h2115] DATE,
[h2225] DATE,
CONSTRAINT pk_Disponibilidade PRIMARY KEY CLUSTERED ([id], [data]),
CONSTRAINT FK_Disponibilidade FOREIGN KEY ([id_formador]) REFERENCES Formador([id])
)

