﻿using filtrandoResultadosBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace filtrandoResultadosBD
{

    //var distinctItems = items.Distinct(new DistinctItemComparer());
    //class DistinctItemComparer : IEqualityComparer<Item>
    //{

    //    public bool Equals(Item x, Item y)
    //    {
    //        return x.Id == y.Id &&
    //            x.Name == y.Name &&
    //            x.Code == y.Code &&
    //            x.Price == y.Price;
    //    }

    //    public int GetHashCode(Item obj)
    //    {
    //        return obj.Id.GetHashCode() ^
    //            obj.Name.GetHashCode() ^
    //            obj.Code.GetHashCode() ^
    //            obj.Price.GetHashCode();
    //    }
    //}
    public class EtapaHorario : IEqualityComparer<EtapaHorario>
    {
        public int IdFormador { get; set; }
        public string NomeFormador { get; set; }
        public DateTime DataInicio { get; set; }
        public int MesFerias { get; set; }
        public int HorasTotais { get; set; }
        public string RegimeTurma { get; set; }
        public List<EtapaHorario> Get()
        {
            using (var db = new ModelCDHContext())
            {
                return (from form in db.formadors

                        join fm in db?.formador_modulo on form.id equals fm.id_formador
                        join t in db?.turmas on fm.id_turma equals t.id
                        join c in db?.cursoes on t.id_curso equals c.id
                        select new EtapaHorario()
                        {
                            IdFormador = form.id,
                            NomeFormador = form.nome,
                            DataInicio = t.data_inicio,
                            MesFerias = t.mes_ferias,
                            HorasTotais = c.TotalHoras,
                            RegimeTurma = t.regime.regime1

                        }).ToList();
            }
        }

        // Um por todos e todos por um
        public bool Equals(EtapaHorario x, EtapaHorario y)
        {
            return x.IdFormador == y.IdFormador;
        }

        public int GetHashCode(EtapaHorario obj)
        {
            return obj.IdFormador.GetHashCode();
        }
    }

    public class InfoParaUMHorario : EqualityComparer<formador_modulo>
    {
        public int IdTurma { get; set; }
        public DateTime DataDeInicio { get; set; }

        //public List<InfoParaUMHorario> Get()
        //{
        //    return(from form in mcdh.formadors
        //                 join fm in mcdh.formador_modulo
        //                 on form.id equals fm.id_formador
        //                 join tm in mcdh.turmas
        //                 on fm.id_turma equals tm.id
        //                 join cur in mcdh.cursoes
        //                 on tm.id_curso equals cur.id
        //                 where form.id == idFormador
        //                 select new InfoParaOHorario
        //                 {
        //                     IdFormador = idFormador,
        //                     NomeFormador = form.nome,
        //                     NmrTotalDeHorasCurso = cur.TotalHoras,
        //                     RegimeTurma = tm.regime.regime1,
        //                     NomeDaTurma = tm.nome,
        //                     DataDeInicio = tm.data_inicio,
        //                     MesDeFerias = tm.mes_ferias
        //                 }).ToList();
        //}

        public override bool Equals(formador_modulo x, formador_modulo y)
        {
            throw new NotImplementedException();
        }

        public override int GetHashCode(formador_modulo obj)
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (ModelCDHContext mcdh = new ModelCDHContext())
            {
                //List<formador> formadorLista = mcdh.formadors.ToList();

                //EtapaHorario eth = new EtapaHorario();

                //var sd = eth.Get();

                //var sd2 = sd.Distinct( new EtapaHorario() );


                //foreach (var item in sd2)
                //{
                //    Console.WriteLine();
                //    Console.WriteLine($"Nome formador: {item.NomeFormador}");
                //    Console.WriteLine($"Data inicio da turma: {item.DataInicio}");
                //    Console.WriteLine($"Mes ferias: {item.MesFerias}");
                //    Console.WriteLine($"Horas totais: {item.HorasTotais}");
                //    Console.WriteLine($"Nome regime: {item.RegimeTurma}");
                //    Console.WriteLine();
                //}
                List<object> asd = new List<object>();
                Console.WriteLine(asd.Count());
                Console.ReadLine();

            }
        }
    }
}
