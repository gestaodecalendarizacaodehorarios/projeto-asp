namespace filtrandoResultadosBD.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class horario_turmas
    {
        [Key]
        [Column(TypeName = "date")]
        public DateTime data { get; set; }

        public int? id_modulo { get; set; }

        public int? id_hora { get; set; }

        public int? id_turma { get; set; }

        public virtual hora hora { get; set; }

        public virtual modulo modulo { get; set; }

        public virtual turma turma { get; set; }
    }
}
