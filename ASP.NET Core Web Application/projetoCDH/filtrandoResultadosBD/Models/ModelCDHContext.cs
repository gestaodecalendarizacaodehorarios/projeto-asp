namespace filtrandoResultadosBD.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelCDHContext : DbContext
    {
        public ModelCDHContext()
            : base("name=ModelCDHContext")
        {
        }

        public virtual DbSet<categoria> categorias { get; set; }
        public virtual DbSet<curso> cursoes { get; set; }
        public virtual DbSet<disponibilidade> disponibilidades { get; set; }
        public virtual DbSet<formador> formadors { get; set; }
        public virtual DbSet<formador_modulo> formador_modulo { get; set; }
        public virtual DbSet<grupo> grupoes { get; set; }
        public virtual DbSet<horario_turmas> horario_turmas { get; set; }
        public virtual DbSet<hora> horas { get; set; }
        public virtual DbSet<modulo> moduloes { get; set; }
        public virtual DbSet<ordem_mod_cat> ordem_mod_cat { get; set; }
        public virtual DbSet<regime> regimes { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<turma> turmas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<categoria>()
                .HasMany(e => e.ordem_mod_cat)
                .WithRequired(e => e.categoria)
                .HasForeignKey(e => e.id_categoria);

            modelBuilder.Entity<curso>()
                .HasMany(e => e.grupoes)
                .WithOptional(e => e.curso)
                .HasForeignKey(e => e.id_curso)
                .WillCascadeOnDelete();

            modelBuilder.Entity<curso>()
                .HasMany(e => e.turmas)
                .WithOptional(e => e.curso)
                .HasForeignKey(e => e.id_curso);

            modelBuilder.Entity<formador>()
                .HasMany(e => e.disponibilidades)
                .WithRequired(e => e.formador)
                .HasForeignKey(e => e.id_formador)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<formador>()
                .HasMany(e => e.formador_modulo)
                .WithRequired(e => e.formador)
                .HasForeignKey(e => e.id_formador);

            modelBuilder.Entity<grupo>()
                .HasMany(e => e.categorias)
                .WithOptional(e => e.grupo)
                .HasForeignKey(e => e.id_grupo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<hora>()
                .HasMany(e => e.horario_turmas)
                .WithOptional(e => e.hora)
                .HasForeignKey(e => e.id_hora)
                .WillCascadeOnDelete();

            modelBuilder.Entity<hora>()
                .HasMany(e => e.regimes)
                .WithMany(e => e.horas)
                .Map(m => m.ToTable("horas_regime").MapLeftKey("id_hora").MapRightKey("id_regime"));

            modelBuilder.Entity<modulo>()
                .HasMany(e => e.formador_modulo)
                .WithRequired(e => e.modulo)
                .HasForeignKey(e => e.id_modulo);

            modelBuilder.Entity<modulo>()
                .HasMany(e => e.horario_turmas)
                .WithOptional(e => e.modulo)
                .HasForeignKey(e => e.id_modulo)
                .WillCascadeOnDelete();

            modelBuilder.Entity<modulo>()
                .HasMany(e => e.ordem_mod_cat)
                .WithRequired(e => e.modulo)
                .HasForeignKey(e => e.id_modulo);

            modelBuilder.Entity<regime>()
                .HasMany(e => e.turmas)
                .WithOptional(e => e.regime)
                .HasForeignKey(e => e.id_regime);

            modelBuilder.Entity<turma>()
                .HasMany(e => e.formador_modulo)
                .WithRequired(e => e.turma)
                .HasForeignKey(e => e.id_turma);

            modelBuilder.Entity<turma>()
                .HasMany(e => e.horario_turmas)
                .WithOptional(e => e.turma)
                .HasForeignKey(e => e.id_turma)
                .WillCascadeOnDelete();
        }
    }
}
