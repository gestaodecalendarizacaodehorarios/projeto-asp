namespace filtrandoResultadosBD.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("turma")]
    public partial class turma
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public turma()
        {
            formador_modulo = new HashSet<formador_modulo>();
            horario_turmas = new HashSet<horario_turmas>();
        }

        public int id { get; set; }

        public int? id_regime { get; set; }

        public int? id_curso { get; set; }

        [Required]
        [StringLength(255)]
        public string nome { get; set; }

        [Column(TypeName = "date")]
        public DateTime data_inicio { get; set; }

        public int mes_ferias { get; set; }

        [Required]
        [StringLength(255)]
        public string coordenador { get; set; }

        public virtual curso curso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<formador_modulo> formador_modulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<horario_turmas> horario_turmas { get; set; }

        public virtual regime regime { get; set; }
    }
}
