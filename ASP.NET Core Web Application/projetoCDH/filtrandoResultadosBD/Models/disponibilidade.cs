namespace filtrandoResultadosBD.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("disponibilidade")]
    public partial class disponibilidade
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_formador { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime data { get; set; }

        public bool h0830 { get; set; }

        public bool h0930 { get; set; }

        public bool h1040 { get; set; }

        public bool h1140 { get; set; }

        public bool h1330 { get; set; }

        public bool h1430 { get; set; }

        public bool h1540 { get; set; }

        public bool h1650 { get; set; }

        public bool h1750 { get; set; }

        public bool h1915 { get; set; }

        public bool h2015 { get; set; }

        public bool h2125 { get; set; }

        public bool h2225 { get; set; }

        public virtual formador formador { get; set; }
    }
}
