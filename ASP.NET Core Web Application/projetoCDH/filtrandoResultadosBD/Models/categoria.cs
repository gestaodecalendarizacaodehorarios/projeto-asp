namespace filtrandoResultadosBD.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("categoria")]
    public partial class categoria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public categoria()
        {
            ordem_mod_cat = new HashSet<ordem_mod_cat>();
        }

        public int id { get; set; }

        public int? id_grupo { get; set; }

        [StringLength(255)]
        public string nome { get; set; }

        public int ordem { get; set; }

        public virtual grupo grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ordem_mod_cat> ordem_mod_cat { get; set; }
    }
}
